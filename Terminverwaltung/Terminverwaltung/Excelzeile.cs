﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

namespace Terminverwaltung
{
    public class Excelzeile
    {
        private string v;
        private Verteilergruppen v1;
        private Lehrers l;
        private Klasses k;
        private Raums r;
        private dynamic s1;
        private dynamic s3;
        private dynamic s4;
        private dynamic s5;
        private dynamic s6;
        private dynamic s7;
        private dynamic s8;
        private dynamic s9;
        private dynamic s10;
        private dynamic s11;
        private dynamic s12;
        private dynamic s13;
        public Klasses Klasses { get; set; }

        public DateTime ADatum { get; set; }
        public string BTag { get; set; }
        public List<DateTime> CVonBis { get; set; }
        public string DBeschreibung { get; set; }
        public string EJahrgang { get; set; }
        public DateTime FBeginn { get; set; }
        public DateTime GEnde { get; set; }
        public Raums HRaum { get; set; }
        public List<object> IVerantwortlich { get; set; }
        public List<string> JKategorie { get; set; }
        public string KHinweise { get; set; }
        public string LGeschützt { get; set; }
        public string MAnmerkungen { get; set; }
        public string Subject { get; set; }
        public string Body { get; private set; }
        public string NSchuljahr { get; set; }
        public string ODetails { get; set; }
        public string DBeschreibungIcs { get; set; }

        public Excelzeile(Verteilergruppen verteilergruppen, Lehrers lehrers, Klasses klasses, Raums raums, DateTime ersterSchultag, string aDatum, string cVonBis, string dBeschreibung, string dBeschreibungIcs, string eJahrgang, string fBeginn, string gEnde, string hRaum, string iVerantwortlich, string jKategorie, string kHinweise, string lGeschützt, string mAnmerkungen, string nSchuljahr, string oDetails, string kategorie)
        {   
            if (aDatum.Contains("-"))
            {
                ADatum = DateTime.ParseExact((aDatum.Split('-')[0]).Trim(), "dd.MM.yyyy", CultureInfo.InvariantCulture);
            }
            else
            {
                ADatum = DateTime.FromOADate(double.Parse(aDatum));
            }
                      
            BTag = "";
            DBeschreibung = dBeschreibung ?? "";
            DBeschreibungIcs = dBeschreibungIcs ?? "";
            EJahrgang = eJahrgang ?? "";
            FBeginn = GetFBegin(fBeginn, aDatum);
            GEnde = GetGEnde(gEnde, aDatum);
            CVonBis = GetBeginnUndEnde(cVonBis, FBeginn, GEnde);
            HRaum = FilterRaum(hRaum, raums);
            IVerantwortlich = FilterVerantwortliche(iVerantwortlich, verteilergruppen, lehrers, klasses);
            JKategorie = GetKategorie(jKategorie, kategorie);
            KHinweise = GetHinweis(jKategorie, kategorie, kHinweise);
            LGeschützt = GetHyperlinks(lGeschützt ?? "");
            MAnmerkungen = mAnmerkungen ?? "";
            NSchuljahr = GetSchuljahr(ersterSchultag, ADatum, nSchuljahr);
            ODetails = oDetails ?? "";
        }

        private string GetHyperlinks(string v)
        {
            foreach (var item in v.Split(' '))
            {
                foreach (var i in item.Split(','))
                {
                    //i = Regex.Replace(i, @"((http|ftp|https):\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?)", "<a target='_blank' href='$1'>$1</a>");


                }
            }
            return v;
        }

        private string GetSchuljahr(DateTime ersterSchultag, DateTime aDatum, string nSchuljahr)
        {
            if (nSchuljahr == null || nSchuljahr == "")
            {
                if (ADatum.Month > 8)
                {
                    return "<b>" + aDatum.Year + "/" + aDatum.AddYears(1).Year.ToString().Substring(2,2) + "</b>";
                }
                else
                {
                    var x = aDatum.AddYears(-1);
                    return "<b>" + (x.Year.ToString()) + "/" + (aDatum.Year.ToString().Substring(2, 2)) + "</b>";
                }
            }
            else
            {
                return nSchuljahr;
            }            
        }

        private string GetHinweis(string jKategorie, string dieseKategorie, string kHinweise)
        {
            var hinweise = kHinweise;

            try
            {
                // Wenn die dieseKategorie ungleich Allgemein ist, ...

                if (dieseKategorie != "Allgemein")
                {

                    // ... aber gleichzeitig die Kategorie Allgemein in der Excelzeile angegeben ist ...

                    if (jKategorie != null && jKategorie.Contains("Allgemein"))
                    {
                        // ... dann wird der Hinweis auf dieseKategorie gesetzt

                        string pfad = dieseKategorie;

                        if (dieseKategorie.ToLower().Contains("konferenz"))
                        {
                            pfad = "konferenzen/" + pfad;
                        }

                        hinweise += "<input type='button' target='_blank' value='" + dieseKategorie + "' title='" + dieseKategorie + "' onclick=\"location.href='/" + dieseKategorie.Replace("ä", "a").Replace("ö", "o").Replace("ü", "u").Replace(" ","-") +"'; \"class='button_link' />";
                    }
                }                   
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return hinweise;
        }

        private DateTime GetGEnde(string gEnde, string aDatum)
        {
            if (gEnde != null)
            {
                gEnde = gEnde.Trim();
            }
            try
            {
                return gEnde == null ? ADatum : ADatum.AddHours(DateTime.ParseExact(gEnde, "HH:mm", CultureInfo.InvariantCulture).Hour).AddMinutes(DateTime.ParseExact(gEnde, "HH:mm", CultureInfo.InvariantCulture).Minute);
            }
            catch (Exception)
            {
                return DateTime.FromOADate(double.Parse(aDatum) + double.Parse(gEnde));
            }
        }

        public DateTime GetFBegin(string fBeginn, string aDatum)
        {
            if (fBeginn != null)
            {
                fBeginn = fBeginn.Trim();
            }
            try
            {
                return DateTime.FromOADate(double.Parse(aDatum) + double.Parse(fBeginn));
            }
            catch (Exception)
            {
                return fBeginn == null ? ADatum : ADatum.AddHours(DateTime.ParseExact(fBeginn, "HH:mm", CultureInfo.InvariantCulture).Hour).AddMinutes(DateTime.ParseExact(fBeginn, "HH:mm", CultureInfo.InvariantCulture).Minute);
            }
        }

        public List<DateTime> GetBeginnUndEnde(string cVonBis, DateTime aDatum, DateTime gEnde)
        {
            var d = new List<DateTime>();

            // Wenn Anfang und Ende gesetzt sind, aber nicht vonBis, dann wird vonBis gesetzt.

            if (aDatum.Year != 0)
            {
                d.Add(aDatum);
            }
            else
            {
                d.Add(new DateTime());
            }
            if (gEnde.Year != 0)
            {
                d.Add(gEnde);
            }
            else
            {
                d.Add(new DateTime());
            }

            // Wenn es sich um eine Uhrzeitangabe handelt ...

            if (cVonBis != null && cVonBis.Contains(":") && cVonBis.Contains("-"))
            {
                int i = 0;

                foreach (var item in cVonBis.Split('-'))
                {
                    // ... und eine gültige Uhrzeitangabe extrahiert werden kann, ...

                    DateTime dd = new DateTime();
                    try
                    {
                        dd = new DateTime(
                            0,
                            0,
                            0,
                            Convert.ToInt32(item.Replace("Uhr", "").Trim().Split(':')[0]),
                            Convert.ToInt32(item.Replace("Uhr", "").Trim().Split(':')[1]),
                            0);

                        // ... dann wird bei vonBis die Ihrzeit gesetzt und ...

                        if (d[i].Hour == 0)
                            d[i].AddHours(Convert.ToInt32(item.Replace("Uhr", "").Trim().Split(':')[0]));
                        if (d[i].Minute == 0)
                            d[i].AddMinutes(Convert.ToInt32(item.Replace("Uhr", "").Trim().Split(':')[1]));

                        // ... beim aDatum wird die Uhrezit ebenfalls gesetzt.

                        if (i == 0 && aDatum.Hour == 0)
                        {
                            aDatum.AddHours(Convert.ToInt32(item.Replace("Uhr", "").Trim().Split(':')[0]));
                            aDatum.AddMinutes(Convert.ToInt32(item.Replace("Uhr", "").Trim().Split(':')[1]));
                        }
                        else
                        {
                            GEnde.AddHours(Convert.ToInt32(item.Replace("Uhr", "").Trim().Split(':')[0]));
                            GEnde.AddMinutes(Convert.ToInt32(item.Replace("Uhr", "").Trim().Split(':')[1]));
                        }

                        i++;
                    }
                    catch (Exception)
                    {
                    }
                }
                return d;
            }

            // Wenn es sich um eine Datumsangabe handelt

            if (cVonBis != null && cVonBis.Contains(".") && cVonBis.Contains("-"))
            {
                int i = 0;

                foreach (var item in cVonBis.Split('-'))
                {
                    DateTime dd = new DateTime();

                    // Wenn es gelingt es Datum zu extrahieren ...
                    try
                    {
                        dd = new DateTime(
                            Convert.ToInt32(item.Replace("Uhr", "").Trim().Split('.')[2]),
                            Convert.ToInt32(item.Replace("Uhr", "").Trim().Split('.')[1]),
                            Convert.ToInt32(item.Replace("Uhr", "").Trim().Split('.')[0]));

                        // wird das auch an Anfang und Ende zugewiesen ...

                        if (i == 0)
                        {
                            ADatum = new DateTime(
                                Convert.ToInt32(item.Replace("Uhr", "").Trim().Split('.')[2].Length == 2 ? "20" + item.Replace("Uhr", "").Trim().Split('.')[2] : item.Replace("Uhr", "").Trim().Split('.')[2]),
                                Convert.ToInt32(item.Replace("Uhr", "").Trim().Split('.')[1]),
                                Convert.ToInt32(item.Replace("Uhr", "").Trim().Split('.')[0]),
                                ADatum.Hour,
                                ADatum.Minute,
                                ADatum.Second);
                            d[i] = ADatum;
                        }

                        if (i == 1)
                        {
                            GEnde = new DateTime(
                                Convert.ToInt32(item.Replace("Uhr", "").Trim().Split('.')[2].Length == 2 ? "20" + item.Replace("Uhr", "").Trim().Split('.')[2] : item.Replace("Uhr", "").Trim().Split('.')[2]),
                                Convert.ToInt32(item.Replace("Uhr", "").Trim().Split('.')[1]),
                                Convert.ToInt32(item.Replace("Uhr", "").Trim().Split('.')[0]),
                                gEnde.Hour,
                                gEnde.Minute,
                                gEnde.Second);
                            d[i] = GEnde;
                        }
                    }
                    catch (Exception) { }
                    i++;
                }
            }
            return d;
        }

        private List<string> GetKategorie(string jKategorie, string dieseKategorie)
        {
            var kat = new List<string>();
            try
            {
                // Wenn bereits die Kategorie "Allgemein" gesetzt ist, dann wird dieseKategorie nicht hinzugefügt, außer ...
                
                if (jKategorie != null && jKategorie.Contains("Allgemein"))
                {
                    // ... bei beweglichen Ferientagen

                    if (dieseKategorie.StartsWith("Bewegliche") || dieseKategorie.ToLower().StartsWith("unterrichtsfrei"))
                    {   
                        kat.Add(dieseKategorie);
                    }
                }
                else
                {
                    if (jKategorie != "unsichtbar")
                    {                        
                        kat.Add(dieseKategorie);
                    }                    
                }

                if (jKategorie == null)
                {
                    return kat;
                }
                foreach (var kategorie in jKategorie.Trim().Split(','))
                {
                    if (kategorie.Length > 1)
                    {
                        kat.Add(kategorie.Trim());
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
            return kat;
        }

        internal void RenderHtmlBeschreibung()
        {
            var regex = new Regex(@"\b[\s,\.-:;]*");            
            
            foreach (var word in regex.Split(DBeschreibung).Where(x => !string.IsNullOrEmpty(x)))
            {
                var klasse = (from k in Klasses where k.NameUntis == word select k).FirstOrDefault();

                if (klasse != null)
                {
                    DBeschreibung = DBeschreibung.Replace(word, "<b><a href='" + klasse.Url + "'>" + klasse.NameUntis + "</a></b>");
                }
            }
        }

        public Excelzeile(DateTime aDatum, string bTag, string cVonBis, string dBeschreibung, string eJahrgang, DateTime fBeginn, DateTime gEnde, Raums hRaum, List<object> iVerantwortlich, List<string> jKategorie, string kHinweis, string lGeschützt, string mAnmerkungen, string nSchuljahr, string oDetails)
        {
            CVonBis = GetBeginnUndEnde(cVonBis, ADatum, gEnde);
            ADatum = aDatum;
            BTag = bTag;
            DBeschreibung = dBeschreibung;
            EJahrgang = eJahrgang;
            FBeginn = fBeginn;
            GEnde = gEnde;
            HRaum = hRaum;
            IVerantwortlich = iVerantwortlich;
            JKategorie = jKategorie;
            KHinweise = kHinweis;
            LGeschützt = lGeschützt;
            MAnmerkungen = mAnmerkungen;
            NSchuljahr = nSchuljahr;
            ODetails = oDetails;
        }

        public Excelzeile()
        {
        }

        public Excelzeile(DateTime aDatum, DateTime fBeginn, DateTime gEnde, string subject, string body)
        {
            ADatum = aDatum;
            FBeginn = fBeginn;
            GEnde = gEnde;
            Subject = subject;

            Body = body;
        }

        public Excelzeile(Klasses klasses)
        {
            Klasses = klasses;
        }

        private Raums FilterRaum(string hRaum, Raums raums)
        {
            Raums rs = new Raums();

            if (hRaum == null)
            {
                return rs;
            }

            foreach (var item in hRaum.Split(','))
            {
                if (item.Trim() == "1003")
                {
                    rs.Add(new Raum("1003"));
                }
                else
                {
                    bool treffer = false;
                    foreach (var r in raums)
                    {
                        if (item.Trim() == r.Raumnummer)
                        {
                            rs.Add(r);
                            treffer = true;
                        }
                    }

                    if (hRaum != "" && !treffer)
                    {
                        rs.Add(new Raum(hRaum));
                    }
                }
            }

            return rs;
        }

        public List<object> FilterVerantwortliche(string iVerantwortlich, Verteilergruppen verteilergruppen, Lehrers lehrers, Klasses klasses)
        {
            List<object> verantwortliche = new List<object>();

            if (iVerantwortlich == null)
            {
                return verantwortliche;
            }
            foreach (var l in lehrers)
            {
                if (l.Mail != "")
                {
                    if (!(iVerantwortlich.Contains("GE") && iVerantwortlich.Contains("GW") && iVerantwortlich.Contains("GT")))
                    {
                        {
                            if (iVerantwortlich == l.Kürzel ||
                                    (from i in iVerantwortlich.Split(',')
                                     where i.Trim() == l.Kürzel
                                     select i).Any() ||
                                     iVerantwortlich.Contains(l.Anrede + " " + l.Nachname) ||
                                     iVerantwortlich.Contains(l.Mail) ||
                                     iVerantwortlich.Contains(">" + l.Kürzel + "<") ||
                                        iVerantwortlich.Contains(">" + l.Nachname + "<"))
                            {
                                verantwortliche.Add(l);

                                iVerantwortlich = iVerantwortlich.Replace(l.Kürzel, "");
                            }                                
                        }
                    }
                }
            }

            foreach (var item in iVerantwortlich.Trim().Split(','))
            {
                var getrimmt = item.Trim();

                var treffer = false;

                foreach (var k in klasses)
                {
                    if (k.NameUntis == getrimmt)
                    {
                        verantwortliche.Add(k);
                        treffer = true;
                        break;
                    }
                }

                if (!treffer)
                {
                    foreach (var k in klasses)
                    {
                        // Der Zähler ist immer 1stellig
                        if (k.NameUntis.EndsWith("11") || k.NameUntis.EndsWith("12") || k.NameUntis.EndsWith("13"))
                        {
                            if (k.NameUntis == getrimmt)
                            {
                                verantwortliche.Add(k);
                                treffer = true;
                            }
                        }
                        else
                        {
                            string pattern = @"\d+$";
                            string replacement = "";
                            Regex rgx = new Regex(pattern);
                            string result = rgx.Replace(k.NameUntis, replacement);

                            if (rgx.Replace(getrimmt, replacement) == rgx.Replace(k.NameUntis, replacement))
                            {
                                verantwortliche.Add(k);
                                treffer = true;
                            }
                        }
                    }
                }

                if (!treffer)
                {
                    foreach (var v in verteilergruppen)
                    {
                        if (getrimmt == v.Name || (getrimmt == v.Mail && v.Mail != ""))
                        {
                            verantwortliche.Add(v);
                            treffer = true;
                            break;
                        }
                    }
                }
                if (!treffer && getrimmt != "")
                {
                    verantwortliche.Add(getrimmt);
                }
            }

            return verantwortliche;
        }

        public string GetPraktikumBeschreibung(Feriens feriens)
        {
            int anzahl = 0;
            // Anfang direkt nach den Ferien?

            string nachFerien = feriens.BeginnDirektNachFerien(ADatum);

            // Ende direkt vor Ferien?
            
            string vorFerien = feriens.BeginnDirektNVorFerien(ADatum);

            // Bei Prakita wwerden die Kalenderwochen herausgesucht

            string kwstring = "Kalenderwochen: ";
            int kw = 0;

            for (DateTime date = ADatum; date.Date <= GEnde.Date; date = date.AddDays(1))
            {
                if (kw != GetKalenderwoche(date))
                {
                    kw = GetKalenderwoche(date);
                    kwstring += kw + ",";
                    anzahl++;
                }
            }
            kwstring = kwstring.TrimEnd(',');

            //if (nachFerien != "")
            //{
            //    kwstring += ";<br>im Anschluss an: " + nachFerien;
            //}
            //if (vorFerien != "")
            //{
            //    kwstring += ";<br>anschließend: " + vorFerien;
            //}
            KHinweise = (anzahl == 1 ? kwstring.Replace("Kalenderwochen", "Kalenderwoche") : kwstring);
            DBeschreibung += "<br>(" + (anzahl == 1 ? anzahl + " Woche)" : anzahl + " Wochen)");
            return DBeschreibung;
        }

        private int GetKalenderwoche(DateTime datum)
        {
            CultureInfo CUI = CultureInfo.CurrentCulture;
            return CUI.Calendar.GetWeekOfYear(datum, CUI.DateTimeFormat.CalendarWeekRule, CUI.DateTimeFormat.FirstDayOfWeek);
        }
    }
}
    