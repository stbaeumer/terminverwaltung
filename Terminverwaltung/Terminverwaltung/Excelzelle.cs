﻿namespace Terminverwaltung
{
    public class Excelzelle
    {
        public int Zeile { get; set; }
        public int Spalte { get; set; }
        public string Inhalt { get; set; }

        public Excelzelle(int zeile, int spalte, string inhalt)
        {
            Zeile = zeile;
            Spalte = spalte;
            Inhalt = inhalt;
        }
    }
}