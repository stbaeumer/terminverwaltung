﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Globalization;
using System.Linq;

namespace Terminverwaltung
{
    public class Unterrichtsgruppes : List<Unterrichtsgruppe>
    {
        public Unterrichtsgruppes()
        {
            using (OleDbConnection oleDbConnection = new OleDbConnection(Global.ConU))
            {
                try
                {
                    string queryString = @"SELECT DISTINCT
LessonGroup.LESSON_GROUP_ID, 
LessonGroup.Name,
LessonGroup.DateFrom,
LessonGroup.DateTo,
LessonGroup.InrerruptionsFrom,
LessonGroup.InrerruptionsTo
FROM LessonGroup
WHERE (((SCHOOLYEAR_ID)= " + Global.AktSjUnt + ") AND ((LessonGroup.SCHOOL_ID)=177659)) ORDER BY LESSON_GROUP_ID;";

                    OleDbCommand oleDbCommand = new OleDbCommand(queryString, oleDbConnection);
                    oleDbConnection.Open();
                    OleDbDataReader oleDbDataReader = oleDbCommand.ExecuteReader();

                    Console.WriteLine("");
                    Console.WriteLine("Unterrichtsgruppen");
                    Console.WriteLine("------------------");

                    while (oleDbDataReader.Read())
                    {
                        Interruption interruption = new Interruption();

                        foreach (var date in (Global.SafeGetString(oleDbDataReader, 4)).Split(','))
                        {
                            if (date != "")
                            {
                                interruption.von.Add(DateTime.ParseExact(date, "yyyyMMdd", CultureInfo.InvariantCulture));
                            }
                        }

                        foreach (var date in (Global.SafeGetString(oleDbDataReader, 5)).Split(','))
                        {
                            if (date != "")
                            {
                                interruption.bis.Add(DateTime.ParseExact(date, "yyyyMMdd", CultureInfo.InvariantCulture));
                            }
                        }

                        // Nach DateTo und vor DateFrom wird alles zur Interruption

                        interruption.von.Add(new DateTime(Convert.ToInt32(Global.AktSjUnt.Substring(0, 4)), 8, 1));
                        interruption.bis.Add(DateTime.ParseExact((oleDbDataReader.GetInt32(2)).ToString(), "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture));

                        interruption.von.Add(DateTime.ParseExact((oleDbDataReader.GetInt32(3)).ToString(), "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture));
                        interruption.bis.Add(new DateTime(Convert.ToInt32(Global.AktSjUnt.Substring(4, 4)), 7, 31));

                        Unterrichtsgruppe unterrichtsgruppe = new Unterrichtsgruppe()
                        {
                            IdUntis = oleDbDataReader.GetInt32(0),
                            Name = Global.SafeGetString(oleDbDataReader, 1),
                            Von = DateTime.ParseExact((oleDbDataReader.GetInt32(2)).ToString(), "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture),
                            Bis = DateTime.ParseExact((oleDbDataReader.GetInt32(3)).ToString(), "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture),
                            Interruption = interruption
                        };

                        Console.WriteLine(" " + unterrichtsgruppe.IdUntis.ToString().PadLeft(3) + " " + unterrichtsgruppe.Name.PadRight(8) + " " + unterrichtsgruppe.Von.ToShortDateString() + " - " + unterrichtsgruppe.Bis.ToShortDateString());

                        // Bei 1.HJ werden alle Unterrichte des 2.HJ als Interruption eingetragen

                        if (unterrichtsgruppe.Name == "1.HJ")
                        {
                            unterrichtsgruppe.Interruption.von.Add(unterrichtsgruppe.Bis);
                            unterrichtsgruppe.Interruption.bis.Add(new DateTime(unterrichtsgruppe.Bis.Year, 7, 31));
                        }

                        if (unterrichtsgruppe.Name == "2.HJ")
                        {
                            unterrichtsgruppe.Interruption.von.Add(new DateTime(unterrichtsgruppe.Von.AddYears(-1).Year, 8, 1));
                            unterrichtsgruppe.Interruption.bis.Add(unterrichtsgruppe.Von.AddDays(-1));
                        }

                        if (unterrichtsgruppe.Name == "U")
                        {
                            for (DateTime date = unterrichtsgruppe.Von; date.Date <= unterrichtsgruppe.Bis; date = date.AddDays(1))
                            {
                                int kw = (CultureInfo.CurrentCulture).Calendar.GetWeekOfYear(date, (CultureInfo.CurrentCulture).DateTimeFormat.CalendarWeekRule, (CultureInfo.CurrentCulture).DateTimeFormat.FirstDayOfWeek);

                                if (date.DayOfWeek == DayOfWeek.Monday)
                                {
                                    if (kw % 2 == 0)
                                    {
                                        unterrichtsgruppe.Interruption.von.Add(date);
                                    }
                                }
                                if (date.DayOfWeek == DayOfWeek.Sunday && unterrichtsgruppe.Interruption.von.Count > 0)
                                {
                                    if (kw % 2 == 0)
                                    {
                                        unterrichtsgruppe.Interruption.bis.Add(date);
                                    }
                                }
                            }
                        }

                        if (unterrichtsgruppe.Name == "G")
                        {
                            for (DateTime date = unterrichtsgruppe.Von; date.Date <= unterrichtsgruppe.Bis; date = date.AddDays(1))
                            {
                                int kw = (CultureInfo.CurrentCulture).Calendar.GetWeekOfYear(date, (CultureInfo.CurrentCulture).DateTimeFormat.CalendarWeekRule, (CultureInfo.CurrentCulture).DateTimeFormat.FirstDayOfWeek);

                                if (date.DayOfWeek == DayOfWeek.Monday)
                                {
                                    if (kw % 2 == 1)
                                    {
                                        unterrichtsgruppe.Interruption.von.Add(date);
                                    }
                                }
                                if (date.DayOfWeek == DayOfWeek.Sunday && unterrichtsgruppe.Interruption.von.Count > 0)
                                {
                                    if (kw % 2 == 1)
                                    {
                                        unterrichtsgruppe.Interruption.bis.Add(date);
                                    }
                                }
                            }
                        }

                        this.Add(unterrichtsgruppe);
                    };
                    oleDbDataReader.Close();
                    Console.WriteLine("");

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    throw new Exception(ex.ToString());
                }
                finally
                {
                    oleDbConnection.Close();
                }
            }
        }

        internal List<Excelzeile> Blockpläne(Klasses klasses, Unterrichts unterrichts, Feriens feriens, DateTime ersterSchultag, Excelzeilen excel, List<string> Block)
        {
            List<Excelzeile> excelzeilen = new List<Excelzeile>();

            try
            {
                // Jede Unterrichtsgruppe steht für eine Blockklassen Jahrgangsstufe

                foreach (var ug in this)
                {
                    if (ug.Interruption != null && Block.Contains(ug.Name))
                    {
                        Klasses kls = new Klasses();

                        foreach (var u in unterrichts)
                        {
                            if (u.Unterrichtsgruppe != null)
                            {
                                if (ug.Name == u.Unterrichtsgruppe.Name)
                                {
                                    var z = (from k in klasses
                                             where k.NameUntis != null
                                             where k.NameUntis == u.KlasseKürzel
                                             select k).FirstOrDefault();

                                    if (z != null)
                                    {
                                        if (!(from k in kls where k.NameUntis == z.NameUntis select z).Any())
                                        {
                                            kls.Add(z);
                                        }
                                    }
                                }
                            }
                        }

                        if (kls.Count > 0)
                        {
                            string inhalt = GetInhalt(kls, ug);

                            // Der erste Tag nach einer Interruption oder einem Feiertag oder einem Ferientag ist der Beginn des Blocks.

                            List<DateTime> a = new List<DateTime>();
                            for (DateTime date = new DateTime(ersterSchultag.Year, 8, 1); date <= new DateTime(ersterSchultag.Year + 1, 7, 31); date = date.AddDays(1))
                            {
                                bool i = LiegtInEinerInterruption(date, ug);
                                bool f = LiegtInDenFerien(date, feriens);
                                bool w = LiegtImWochenende(date);

                                if (!i && !f && !w)
                                {
                                    a.Add(date);
                                }
                            }

                            // Jetzt werden die zusammenhängenden Zeiträume aufgeteilt

                            DateTime ersterTag = new DateTime();
                            DateTime letzterTag = new DateTime();
                            List<int> kalenderwochen = new List<int>();
                            int anzahlschultage = 0;
                            int anzahlWochen = 0;

                            for (int i = 0; i < a.Count; i++)
                            {
                                DateTime dieserTag = a[i];
                                DateTime vorgänger = a[Math.Max(i - 1, 0)];
                                DateTime nachfolger = a[Math.Min(a.Count - 1, i + 1)];

                                // Alle Tage, deren Vorgänger 7 oder mehr Tage entfernt sind, sind Blockbeginn.

                                if (i == 0 || dieserTag.AddDays(-6) > vorgänger)
                                {
                                    ersterTag = new DateTime();
                                    ersterTag = dieserTag;
                                }

                                // Alle Tage, deren Nachfolger 7 oder mehr Tage entfernt ist, sind Blockende

                                if (i == a.Count - 1 || dieserTag.AddDays(6) < nachfolger)
                                {
                                    letzterTag = new DateTime();
                                    letzterTag = dieserTag;
                                }

                                if (ersterTag.Year != 1)
                                {
                                    anzahlschultage++;
                                }

                                if (((dieserTag - ersterTag).TotalDays) % 7 == 0)
                                {
                                    anzahlWochen++;
                                    kalenderwochen.Add(GetKalenderwoche(dieserTag));
                                }
                                if (ersterTag.Year != 1 && letzterTag.Year != 1 && (ersterTag.AddDays(3) < letzterTag))
                                {
                                    string dBeschreibung = "<b>" + inhalt + "</b>";
                                    //string dBeschreibung = "<b><a href='" + klasse.Url + "'>" + klasse.NameUntis + " - " + klasse.Beschreibung + " - " + GetStufe(klasse) + "</a></b>";
                                    string eJahrgang = (kls[0].Jahrgang.EndsWith("1") ? "Unterstufe" : kls[0].Jahrgang.EndsWith("2") ? "Mittelstufe" : kls[0].Jahrgang.EndsWith("3") ? "Oberstufe" : "Abschlussklasse"); ;

                                    DateTime fBeginn = ersterTag;
                                    DateTime gEnde = letzterTag;
                                    Raums hRaum = new Raums();

                                    Console.WriteLine("Block " + inhalt + " " + eJahrgang + " " + ersterTag.ToString() + " - " + letzterTag.ToString() + " Schultage: " + anzahlschultage + " Wochen: " + anzahlWochen);

                                    var iVerantwortlich = new List<object>();

                                    for (int k = 0; k < kls.Count; k++)
                                    {
                                        iVerantwortlich.Add(kls[k]);
                                    }
                                    List<string> jKategorie = new List<string>() { "Blockzeit", "Blockzeit " + ug.Name.Substring(0, 1) };

                                    if (!(from e in excelzeilen
                                          where e.DBeschreibung == dBeschreibung
                                          select e).Any())
                                    {
                                        if (ug.Name.EndsWith("U"))
                                        {
                                            excelzeilen.Add(
                                            new Excelzeile(
                                                fBeginn,
                                                "",
                                                fBeginn.ToString("dd.MM.yyyy") + "-" + gEnde.ToString("dd.MM.yyyy"),
                                                dBeschreibung,
                                                eJahrgang,
                                                fBeginn,
                                                gEnde,
                                                hRaum,
                                                new List<object>(),
                                                jKategorie,
                                                "<a href='https://berufskolleg-borken.de/Termine_Dateien/Blockzeiten/" + ug.Name + ".pdf" + "' target='_blank'><img src ='https://berufskolleg-borken.de/Termine_Dateien/pdf.png" + "' alt='Blockplan'></a>",
                                                //(kalenderwochen[0] == kalenderwochen[kalenderwochen.Count - 1] ? "" : kalenderwochen[0].ToString("00") + ".-") + kalenderwochen[kalenderwochen.Count - 1].ToString("00") + ".KW (" + anzahlWochen + " Woche" + (kalenderwochen[0] == kalenderwochen[kalenderwochen.Count - 1] ? "" : "n") + ")", 
                                                "",
                                                "",
                                                "",
                                                "")
                                                );
                                        }
                                        ersterTag = new DateTime();
                                        letzterTag = new DateTime();
                                        anzahlWochen = 0;
                                        anzahlschultage = 0;
                                        kalenderwochen = new List<int>();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Console.ReadKey();
            }

            return (from e in excelzeilen select e).OrderBy(x => x.DBeschreibung).ThenBy(x => x.FBeginn).ToList();
        }

        private int GetKalenderwoche(DateTime Datum)
        {
            CultureInfo CUI = CultureInfo.CurrentCulture;
            return CUI.Calendar.GetWeekOfYear(Datum, CUI.DateTimeFormat.CalendarWeekRule, CUI.DateTimeFormat.FirstDayOfWeek);
        }

        private bool LiegtImWochenende(DateTime dayToday)
        {
            if ((dayToday.DayOfWeek == DayOfWeek.Saturday) || (dayToday.DayOfWeek == DayOfWeek.Sunday))
            {
                return true;
            }
            return false;
        }

        private bool LiegtInDenFerien(DateTime date, Feriens feriens)
        {
            foreach (var ferien in feriens)
            {
                if (ferien.Von <= date && date <= ferien.Bis)
                {
                    return true;
                }
            }
            return false;
        }

        private bool LiegtInEinerInterruption(DateTime date, Unterrichtsgruppe ug)
        {
            for (int i = 0; i < ug.Interruption.von.Count; i++)
            {
                if (ug.Interruption.von[i] <= date && date < ug.Interruption.bis[i])
                {
                    return true;
                }
            }
            return false;
        }

        private string GetInhalt(Klasses c, Unterrichtsgruppe unterrichtsgruppe)
        {
            string a = "";
            
            for (int i = 0; i < c.Count(); i++)
            {
                if (i == 0 || (i > 0 && c[i].Beschreibung != c[i-1].Beschreibung))
                {
                    a += " <a href='" + c[i].Url + "'>" + c[i].Beschreibung + "</a> |";
                }                
            }

            a = a.TrimEnd('|');
            
            return a;
        }
    }
}