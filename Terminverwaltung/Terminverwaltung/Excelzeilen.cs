﻿using Ical.Net;
using Ical.Net.DataTypes;
using Ical.Net.Serialization;
using Microsoft.Exchange.WebServices.Data;
using Microsoft.Office.Interop.Excel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;

namespace Terminverwaltung
{
    public class Excelzeilen : List<Excelzeile>
    {
        public Klasses Klasses { get; set; }
        
        public string Dateiname { get; private set; }
        public string Kopfzeile { get; private set; }
        public List<string> ColumnNames { get; set; }

        public Excelzeilen(string dateiname, Lehrers lehrers, Klasses klasses, Verteilergruppen verteilergruppen, Raums raums, DateTime ersterSchultag)
        {
            int zeile = 0;

            if (File.Exists(Global.TerminePfad + dateiname + ".xlsx"))
            {
                Global.WaitForFile(Global.TerminePfad + dateiname + ".xlsx");
            }
            else
            {
                Console.WriteLine("Die Datei " + Global.TerminePfad + dateiname + ".xlsx soll jetzt eingelesen werden, kann aber nicht gefunden werden.");                
            }
            
            Application excel = new Application();
            Workbook workbook = excel.Workbooks.Open(Global.TerminePfad + dateiname + ".xlsx");
            Worksheet worksheet = (Worksheet)workbook.Worksheets.get_Item(1);
            Range xlRange = worksheet.UsedRange;

            try
            {
                Dateiname = Global.TerminePfad + dateiname + ".xlsx";

                Console.Write("Lese Exceldatei " + Path.GetFileName(Global.TerminePfad + dateiname + ".xlsx") + " ... ");

                int rowCount = xlRange.Rows.Count;

                using (var progress = new ProgressBar())
                {
                    for (int i = 2; i < rowCount + 1; i++)
                    {
                        zeile = i;
                        var v = verteilergruppen;
                        var l = lehrers;
                        var k = klasses;
                        var r = raums;

                        int columnCount = worksheet.UsedRange.Columns.Count;

                        Global.ColumnNames = new List<string>();

                        for (int c = 1; c <= columnCount; c++)
                        {
                            if (worksheet.Cells[1, c].Value2 != null)
                            {
                                Global.ColumnNames.Add(Convert.ToString(worksheet.Cells[1, c].Value2));
                            }
                        }

                        if (Global.Zwischenablage == "")
                        {
                            Global.Zwischenablage = ZwischenablageKopfzeile();
                        }
                        
                        if (xlRange.Cells[i, 1].Value2 != null)
                        {
                            var s1 = xlRange.Cells[i, 1].Value2.ToString();
                            var s3 = Convert.ToString(xlRange.Cells[i, 3].Value2);
                            
                            bool bold = false;
                            bool italic = false;
                            int underline = -4142;
                            Color color = Color.FromArgb(0, 0, 0, 0); // Schwarz

                            StringBuilder html = new StringBuilder();
                            
                            for (int index1 = 1; index1 <= xlRange.Cells[i, 4].Text.ToString().Length; index1++)
                            {
                                Characters ch = xlRange.Cells[i, 4].Characters(index1, 1);

                                var argb = Convert.ToInt32(ch.Font.Color);
                                var col = Color.FromArgb(argb);
                                
                                if (col != color)
                                {
                                    color = col;

                                    if (col.R != 0 || col.G != 0 || col.B != 0)
                                    {                                        
                                        html.Append("<span style='color: rgb(" + col.R + ", " + col.G + ", " + col.B + ")'>");
                                    }
                                    else
                                    {
                                        html.Append("</span>");
                                    }
                                }

                                if (ch.Font.Underline != underline)
                                {
                                    underline = ch.Font.Underline;

                                    if (ch.Font.Underline == 2)
                                    {
                                        html.Append("<u>");
                                    }
                                    else
                                    {
                                        html.Append("</u>");
                                    }
                                }

                                if ((bool)ch.Font.Bold != bold)
                                {
                                    bold = !bold;

                                    if ((bool)ch.Font.Bold)
                                    {
                                        html.Append("<b>");
                                    }
                                    else
                                    {
                                        html.Append("</b>");
                                    }
                                }
                                if ((bool)ch.Font.Italic != italic)
                                {
                                    italic = !italic;

                                    if ((bool)ch.Font.Italic)
                                    {
                                        html.Append("<i>");
                                    }
                                    else
                                    {
                                        html.Append("</i>");
                                    }
                                }
                                if (ch.Text == "\n")
                                {
                                    html.Append("<br>");
                                }
                                else
                                {
                                    html.Append(ch.Text);
                                }
                            }

                            if (bold == true)
                            {
                                html.Append("</b>");
                            }
                            if (underline != -4142)
                            {
                                html.Append("</u>");
                            }
                            if (italic == true)
                            {
                                html.Append("</i>");
                            }

                            //if (xlRange.Cells[i, 4].Cells.Hyperlinks[1].Address != null)
                            //{
                            //    html = "<a href='" + xlRange.Cells[i, 4].Cells.Hyperlinks[1].Address + "'>" + html.ToString() + "</a>";
                            //}

                            

                            var s4 = html.ToString();
                            var s41 = xlRange.Cells[i, 4].Text.ToString();
                            var s5 = Convert.ToString(xlRange.Cells[i, 5].Value2);
                            var s6 = Convert.ToString(xlRange.Cells[i, 6].Value2);
                            var s7 = Convert.ToString(xlRange.Cells[i, 7].Value2);
                            var s8 = Convert.ToString(xlRange.Cells[i, 8].Value2);
                            var s9 = Convert.ToString(xlRange.Cells[i, 9].Value2);
                            var s10 = Convert.ToString(xlRange.Cells[i, 10].Value2);
                            var s11 = Convert.ToString(xlRange.Cells[i, 11].Value2) ?? ""; // Hinweise
                            var s12 = Convert.ToString(xlRange.Cells[i, 12].Value2);
                            var s13 = Convert.ToString(xlRange.Cells[i, 13].Value2);
                            var s14 = Convert.ToString(xlRange.Cells[i, 14].Value2);
                            var s15 = Convert.ToString(xlRange.Cells[i, 15].Value2);

                            Add(new Excelzeile(v, l, k, r, ersterSchultag, s1, s3, s4, s41, s5, s6, s7, s8, s9, s10, s11, s12, s13, s14, s15, dateiname));                 
                        }
                        progress.Report((double)i / rowCount);                        
                    }            
                }

                workbook.Close(0);
                excel.Quit();
                
                Console.WriteLine( "Fertig.");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Fehler in Zeile " + zeile);
                Console.WriteLine(ex.ToString());
                workbook.Close(0);
                excel.Quit();
                Console.ReadKey();
                Environment.Exit(0);
            }
        }

        internal void Voreinschulung(Excelzeilen excelzeilenQuelle, Lehrers lehrers, Klasses klasses)
        {
            foreach (var e in excelzeilenQuelle.OrderBy(x => x.FBeginn))
            {
                var ansprechpartner = "Auskunft erteilt: ";
                                
                foreach (var lehrer in lehrers)
                {
                    foreach (var l in e.IVerantwortlich)
                    {
                        if (l.GetType() == typeof(Lehrer))
                        {
                            if (((Lehrer)l).Kürzel == lehrer.Kürzel)
                            {                
                                ansprechpartner += "<a title='Nachricht für " + GetAnrede(lehrer) + "' href='mailto:" + lehrer.Mail + "?subject=Nachricht für " + GetAnrede(lehrer) + "'>" + "<b>" + lehrer.Anrede + " " + lehrer.Titel + (lehrer.Titel == "" ? "" : " ") + lehrer.Nachname + " </b></a>";
                            }
                        }                    
                    }
                }
                
                Klasse kl = klasses.GetKlasseFromString(e.DBeschreibung);
                
                var excelzeile = new Excelzeile(Klasses)
                {
                    ADatum = e.ADatum,
                    BTag = e.BTag,
                    DBeschreibung = "<b>Voreinschulung " + (!e.DBeschreibung.Contains("Voreinschulung") ?  "<a href='"  + kl.Url + "'>" + kl.Beschreibung + "</a>" : "") + "</b>" + (e.MAnmerkungen != "alle" && !e.DBeschreibung.Contains("Voreinschulung") ? "</br><b>Achtung</b>: Die Gruppe wird aufgeteilt. Zu diesem Termin bitten wir die Schülerinnen und Schüler mit den Nachnamen beginnend mit: " + e.MAnmerkungen : "") + (!e.DBeschreibung.Contains("Voreinschulung") ? "</br>" + ansprechpartner.TrimEnd() : ""),
                    EJahrgang = e.EJahrgang,
                    FBeginn = e.FBeginn,
                    GEnde = e.GEnde,
                    CVonBis = e.CVonBis,
                    HRaum = e.HRaum,
                    IVerantwortlich = e.IVerantwortlich,
                    JKategorie = e.JKategorie,
                    KHinweise = e.KHinweise,
                    LGeschützt = e.LGeschützt,
                    MAnmerkungen = e.MAnmerkungen
                };
                                
                this.Add(excelzeile);
            }
        }

        internal void RenderHtml()
        {
            foreach (var eS in this)
            {
                if (eS.JKategorie.Contains("Voreinschulung"))
                {                    
                    eS.RenderHtmlBeschreibung();
                }
            }
        }

        internal void ToIcs(Lehrers lehrers, Klasses klasses, Verteilergruppen verteilergruppen, Raums raums, Feriens feriens)
        {
            try
            {
                // Alle relevanten Termine heraussuchen

                Excelzeilen relevant = new Excelzeilen();

                relevant.AddRange((from x in this
                                   where (x.ADatum.Date >= DateTime.Now.Date && x.ADatum.Date < Global.LetzterTagDesSchuljahres.AddDays(20)) ||
                                   x.JKategorie.Contains("unterrichtsfrei")
                                   select x).ToList());

                var calendar = new Ical.Net.Calendar();

                int z = 0;
                int i = 0;

                for (i = 0; i < relevant.Count; i++)
                {
                    //Repeat daily for 5 days
                    //var rrule = new RecurrencePattern(FrequencyType.Daily, 1) { Count = 5 };

                    var e = new Ical.Net.CalendarComponents.CalendarEvent();

                    e.Start = new CalDateTime(relevant[i].FBeginn);

                    // to make an all day event you need to make the appointment end at midnight the day after.

                    if (relevant[i].FBeginn == relevant[i].GEnde)
                    {
                        var sz = "";
                    }
                    else
                    {
                        e.End = new CalDateTime(relevant[i].GEnde);
                    }                   
                    
                    

                    e.Organizer = new Organizer()
                    {
                        CommonName = "BKB-Terminplanung",
                        Value = new Uri("mailto:stefan.baeumer@berufskolleg-borken.de")
                    };

                    if (relevant[i].HRaum != null && relevant[i].HRaum.Count > 0)
                    {
                        e.Location = relevant[i].HRaum[0].Raumname;
                    }

                    var titel = relevant[i].DBeschreibungIcs;

                    if (relevant[i].DBeschreibungIcs != null)
                    {
                        if (relevant[i].DBeschreibungIcs.Contains(@"\n"))
                        {
                            titel = relevant[i].DBeschreibungIcs.Split('\\')[0];
                        }
                    }

                    e.Name = titel;

                    foreach (var kat in relevant[i].JKategorie)
                    {
                        e.Categories.Add(kat);
                    }

                    e.Categories.Add("BKB");
                    e.Description = relevant[i].DBeschreibung;
                    calendar.Events.Add(e);

                    z++;
                }

                var serializer = new CalendarSerializer();
                var serializedCalendar = serializer.SerializeToString(calendar);

                if (File.Exists("C:\\Users\\bm\\Berufskolleg Borken\\Terminplanung - Documents\\ics\\unterrichtsfrei.ics"))
                {
                    File.Delete("C:\\Users\\bm\\Berufskolleg Borken\\Terminplanung - Documents\\ics\\unterrichtsfrei.ics");
                }

                using (StreamWriter outputFile = new StreamWriter("C:\\Users\\bm\\Berufskolleg Borken\\Terminplanung - Documents\\ics\\unterrichtsfrei.ics"))
                {
                    outputFile.Write(serializedCalendar);
                }
                Process myProcess = new Process();
                Process.Start("notepad++.exe", "C:\\Users\\bm\\Berufskolleg Borken\\Terminplanung - Documents\\ics\\unterrichtsfrei.ics");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        internal void Blockzeit(Klasses klasses, Excelzeilen excelzeilenQuelle)
        {
            foreach (var e in excelzeilenQuelle.OrderBy(x => x.FBeginn).ThenBy(y => y.DBeschreibung))
            {
                string beschreibung = "";

                foreach (var item in e.DBeschreibung.Split(';'))
                {
                    try
                    {
                        Klasse kl = (from k in klasses where k.Beschreibung == item.Trim() select k).FirstOrDefault();

                        beschreibung += " " + kl.GetKlasseBeschreibung() + " |";
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Die Beschreibung des Berufes " + item + " aus der Exceldatei entspricht keiner Beschreibung in Untis");
                        
                    }                    
                }

                var excelzeile = new Excelzeile()
                {
                    ADatum = e.ADatum,
                    BTag = e.BTag,
                    DBeschreibung = beschreibung.TrimEnd('|'),// + " -  " + e.MAnmerkungen,
                    EJahrgang = e.EJahrgang,
                    FBeginn = e.FBeginn,
                    GEnde = e.GEnde,
                    CVonBis = e.CVonBis,
                    HRaum = e.HRaum,
                    IVerantwortlich = e.IVerantwortlich,
                    JKategorie = e.JKategorie,
                    KHinweise = "",
                    LGeschützt = e.LGeschützt,
                    MAnmerkungen = e.MAnmerkungen,
                    NSchuljahr = e.NSchuljahr,
                    ODetails = "<a href='https://berufskolleg-borken.de/Termine_Dateien/Blockzeit/" + e.ODetails + "' ><img src='https://berufskolleg-borken.de/Termine_Dateien/pdf.png'></a>",
                };
                this.Add(excelzeile);
            }
        }

        internal void Zulassungskonferenzen(Excelzeilen excelzeilenquelle, Klasses klasses, Lehrers lehrers)
        {
            Excelzeilen excelzeilen = new Excelzeilen();

            foreach (var e in excelzeilenquelle)
            {
                var klasse = (from k in klasses where k.NameUntis == e.DBeschreibung.Trim() select k).FirstOrDefault();

                if (klasse == null)
                {
                    Console.WriteLine("Die Klasse aus der Exceldatei namens " + e.DBeschreibung + " kann nicht gefunden weredn.");
                    Console.ReadKey();
                }

                var teilnehmer = new List<Lehrer>
                    {
                        (from l in lehrers where l.Kürzel == Global.KürzelSchulleiter select l).FirstOrDefault(),
                        (from l in lehrers where l.Kürzel == klasse.Bereichsleitung select l).FirstOrDefault()
                    };

                teilnehmer.AddRange(klasse.Klassenleitungen);
                
                Excelzeile excelzeile = new Excelzeile();

                if (klasse == null)
                {
                    Console.WriteLine("Es fehlt eine Beginn-Uhrzeit in der Datei Zulassungskonferenz.xlsx.");
                    Console.ReadKey();
                }

                var beginn = new DateTime(e.ADatum.Year, e.ADatum.Month, e.ADatum.Day, e.FBeginn.Hour, e.FBeginn.Minute, e.FBeginn.Second);

                excelzeile.ADatum = e.ADatum;
                excelzeile.BTag = string.Format("{0:ddd}", beginn) + " " + beginn.ToString("dd.MM.yyyy");
                excelzeile.CVonBis = new List<DateTime>() { beginn, beginn.AddMinutes(10) };
                excelzeile.DBeschreibung = "Zulassungskonferenz - <a title='Nachricht für " + Global.GetAnrede((klasse).Klassenleitungen[0]) + "' href='mailto: " + klasse.Klassenleitungen[0].Mail + " ?subject=Nachricht für " + Global.GetAnrede(klasse.Klassenleitungen[0]) + "'>" + "<b>" + klasse.NameUntis + "</b></a> - " + klasse.Beschreibung;
                excelzeile.EJahrgang = "";
                excelzeile.FBeginn = beginn;
                excelzeile.GEnde = beginn.AddMinutes(10);
                excelzeile.HRaum = new Raums();
                excelzeile.HRaum.Add(new Raum(Global.RaumApa));
                excelzeile.IVerantwortlich = new List<object>();
                foreach (var item in teilnehmer)
                {
                    excelzeile.IVerantwortlich.Add(item);
                }
                excelzeile.JKategorie = new List<string>() { "ZulassungskonferenzBC " };
                excelzeile.KHinweise = "";
                excelzeile.LGeschützt = "";
                excelzeile.Subject = "Zulassungskonferenz " + klasse.NameUntis;
                excelzeilen.Add(excelzeile);

            }
            
            Excelzeilen xx = new Excelzeilen();

            foreach (var item in (from se in excelzeilen select se).OrderBy(cc => cc.FBeginn))
            {
                xx.Add(item);
            }
            this.AddRange(xx);
        }

        internal void PrüfungsplanungFHR(Excelzeilen excelzeilenQuelle)
        {
            foreach (var e in excelzeilenQuelle.OrderBy(x => x.FBeginn))
            {
                var excelzeile = new Excelzeile()
                {
                    ADatum = e.ADatum,
                    BTag = e.BTag,
                    DBeschreibung = e.DBeschreibung,
                    EJahrgang = e.EJahrgang,
                    FBeginn = e.FBeginn,
                    GEnde = e.GEnde,
                    CVonBis = e.CVonBis,
                    HRaum = e.HRaum,
                    IVerantwortlich = e.IVerantwortlich,
                    JKategorie = e.JKategorie,
                    KHinweise = e.KHinweise,
                    LGeschützt = e.LGeschützt,
                    MAnmerkungen = e.MAnmerkungen
                };
                this.Add(excelzeile);
            }
        }

        internal void Sprechtag(Lehrers lehrers, Unterrichts unterrichts, Raums raums, Klasses klasses, Verteilergruppen verteilergruppen, Excelzeilen excelzeilenQuelle, Anrechnungs anrechnungs)
        {
            var lehrerMitUnterrichtSortiert = (Lehrers)(unterrichts.GetLehrerMitUnterrichtUndMigrationskraftSortiert(lehrers));

            Console.WriteLine("Freie Räume am Sprechtag: \n" + unterrichts.GetFreieRäume(lehrerMitUnterrichtSortiert, raums));

            Console.WriteLine("Mehrfach belegte Räume am Sprechtag: \n" + unterrichts.GetMehrfachbelegteRäume(lehrerMitUnterrichtSortiert));

            Console.WriteLine("In der Excelliste fehlende Lehrer: \n" + lehrerMitUnterrichtSortiert.Fehlende(excelzeilenQuelle));

            Console.WriteLine("Lehrer, die nicht mehr bei uns sind: \n" + excelzeilenQuelle.NichtMehrBeiUns(lehrerMitUnterrichtSortiert));

            try
            {
                DateTime von = new DateTime(excelzeilenQuelle[0].ADatum.Year, excelzeilenQuelle[0].ADatum.Month, excelzeilenQuelle[0].ADatum.Day,13,30,0);

                DateTime bis = new DateTime(excelzeilenQuelle[0].ADatum.Year, excelzeilenQuelle[0].ADatum.Month, excelzeilenQuelle[0].ADatum.Day, 17, 30, 0);

                foreach (var e in excelzeilenQuelle)
                {
                    if ((from s in lehrerMitUnterrichtSortiert
                         where (e.IVerantwortlich != null &&
                         e.IVerantwortlich.Count >= 0 &&
                         e.IVerantwortlich[0].GetType() == typeof(Lehrer) &&
                         ((Lehrer)e.IVerantwortlich[0]).Kürzel == s.Kürzel) || (e.JKategorie != null && e.JKategorie.Count > 0 && e.JKategorie.Contains("Allgemein"))                          select s).Any())
                    {
                        Lehrer l = (from s in lehrerMitUnterrichtSortiert
                                    where e.IVerantwortlich != null
                                    where e.IVerantwortlich.Count >= 0
                                    where e.IVerantwortlich[0].GetType() == typeof(Lehrer)
                                    where ((Lehrer)e.IVerantwortlich[0]).Kürzel == s.Kürzel
                                    select ((Lehrer)e.IVerantwortlich[0])).FirstOrDefault();

                        var b = e.FBeginn.Hour == 0 ? von : e.FBeginn;
                        var en = e.GEnde.Hour == 0 ? bis : e.GEnde;

                        var excelzeile = new Excelzeile()
                        {
                            ADatum = e.ADatum,
                            BTag = e.BTag,
                            FBeginn = b,
                            GEnde = en,
                            CVonBis = e.CVonBis,
                            DBeschreibung = l == null ? e.DBeschreibung : GetSprechtagBeschreibung(l, e.KHinweise, b, en, von, bis),
                            EJahrgang = e.EJahrgang,
                            HRaum = e.HRaum,
                            IVerantwortlich = e.IVerantwortlich,
                            JKategorie = e.JKategorie,
                            KHinweise = e.KHinweise,
                            LGeschützt = e.LGeschützt,
                            MAnmerkungen = e.MAnmerkungen
                        };
                        Add(excelzeile, "Sprechtag");                                                
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Console.ReadKey();
            }
        }

        private void FehlendeLehrerEinsortieren(Lehrers lehrerMitUnterrichtSortiert, DateTime aDtaum, DateTime von, DateTime bis)
        {
            foreach (var item in lehrerMitUnterrichtSortiert)
            {
                var fehlenderLehrer = (from s in this
                                from k in s.IVerantwortlich
                                where k.GetType() == typeof(Lehrer)
                                where ((Lehrer)k).Kürzel == item.Kürzel
                                select ((Lehrer)k)).FirstOrDefault();
                if (fehlenderLehrer == null)
                {
                    // Die Excelzeieln werden durchlaufen ...

                    for (int i = 0; i < this.Count; i++)
                    {
                        // ... bis zu der Stelle, an der der fehlende Lehrer stehen muss.

                        var davor = ((Lehrer)this[i].IVerantwortlich[0]).Kürzel;
                        var dahinter = ((Lehrer)this[Math.Min(i + 1,this.Count)].IVerantwortlich[0]).Kürzel;

                        if (
                            string.Compare(davor, item.Kürzel) == -1 
                            && string.Compare( item.Kürzel, dahinter) == -1) 
                        {
                            var excelzeile = new Excelzeile()
                            {
                                ADatum = aDtaum,
                                BTag = this[i].BTag,
                                DBeschreibung = GetSprechtagBeschreibung(item, "",this[i].FBeginn, this[i].GEnde, von, bis),
                                EJahrgang = this[i].EJahrgang,
                                FBeginn = von,
                                GEnde = bis,
                                CVonBis = this[i].CVonBis,
                                HRaum = this[i].HRaum,
                                IVerantwortlich = this[i].IVerantwortlich,
                                JKategorie = this[i].JKategorie,
                                KHinweise = this[i].KHinweise,
                                LGeschützt = this[i].LGeschützt,
                                MAnmerkungen = this[i].MAnmerkungen
                            };

                            this.Insert(i, excelzeile);
                        }
                    }
                }
            }
        }

        public string GetSprechtagBeschreibung(Lehrer lehrer, string notiz, DateTime eBeginn, DateTime eEnde, DateTime von, DateTime bis)
        {
            if (notiz != null && notiz != "")
            {
                notiz = "            (" + notiz + ")";
            }

            // Abweichende Uhrzeiten werden an die Beschreibung angehangen

            string abweichendeUhrzeit = "";

            if (eBeginn != von && eEnde != bis)
            {
                abweichendeUhrzeit = "<p style='color:#FF0000';>" + eBeginn.Hour.ToString("00") + ":" + eBeginn.Minute.ToString("00") + " Uhr bis " + eEnde.Hour.ToString("00") + ":" + eEnde.Minute.ToString("00") + " Uhr</p>";
            }
            else
            {
                if (eBeginn != von)
                {
                    abweichendeUhrzeit = "<p style='color:#FF0000';>ab " + eBeginn.Hour.ToString("00") + ":" + eBeginn.Minute.ToString("00") + " Uhr</p>";
                }
                if (eEnde != bis)
                {
                    abweichendeUhrzeit = "<p style='color:#FF0000';>bis " + eEnde.Hour.ToString("00") + ":" + eEnde.Minute.ToString("00") + " Uhr</p>";
                }
            }


            return "<a title='Nachricht für " + GetAnrede(lehrer) + "' href='mailto: " + lehrer.Mail + " ?subject=Nachricht für " + GetAnrede(lehrer) + "'>" + "<b>" + lehrer.Anrede + " " + lehrer.Titel + (lehrer.Titel == "" ? "" : " ") + lehrer.Nachname + ", " + lehrer.Kürzel + "</b></a>" + notiz + abweichendeUhrzeit;
        }

        private string GetAnrede(Lehrer lehrer)
        {
            return (lehrer.Anrede == "Frau" ? "Frau" : "Herrn") + " " + lehrer.Titel + (lehrer.Titel == "" ? "" : " ") + lehrer.Nachname;
        }


        public DateTime GetDatum(Excelzeile excelzeile, DateTime aDatum, DateTime date)
        {
            // Wenn es keine Anmerkung gibt (z.B. außer Haus) ...

            if (excelzeile.MAnmerkungen == null || excelzeile.MAnmerkungen == "")
            {
                // ... wird entweder die allgemeine Uhrzeit gesetzt ...

                if (date.Hour == 0)
                {
                    return new DateTime(aDatum.Year, aDatum.Month, aDatum.Day, 13, 30, 0);
                }
                else
                {
                    // ... oder die vorhandene Uhrzeit genommen

                    return date;
                }
            }
            else
            {
                // Wenn es Anmelrungen gibt, wird die Uhrzeit auf null Uhr gesetzt.
                return new DateTime(aDatum.Year, aDatum.Month, aDatum.Day, 0, 0, 0);
            }
        }

        private string NichtMehrBeiUns(Lehrers lehrerMitUnterrichtSortiert)
        {
            string a = "";

            foreach (var item in this)
            {
                try
                {
                    var nicht = (from s in lehrerMitUnterrichtSortiert
                                 where item.IVerantwortlich != null
                                 where item.IVerantwortlich.Count >= 0
                                 where item.IVerantwortlich[0].GetType() == typeof(Lehrer)
                                 where ((Lehrer)item.IVerantwortlich[0]).Kürzel == s.Kürzel
                                 select s).FirstOrDefault();
                    if (nicht == null
                        && item.IVerantwortlich != null
                        && item.IVerantwortlich.Count >= 0
                        && item.IVerantwortlich[0].GetType() == typeof(Lehrer))
                    {
                        a += ((Lehrer)item.IVerantwortlich[0]).Kürzel + ",";
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }                
            }
            return a.TrimEnd(',');
        }

        internal void Unterrichtsfrei(Excelzeilen excelzeilenQuelle, Feriens feriens)
        {
            foreach (var e in excelzeilenQuelle.OrderBy(x => x.FBeginn))
            {                
                var excelzeile = new Excelzeile()
                {
                    ADatum = e.ADatum,
                    BTag = e.BTag,
                    DBeschreibung = e.DBeschreibung,
                    DBeschreibungIcs = e.DBeschreibungIcs,
                    EJahrgang = e.EJahrgang,
                    FBeginn = e.FBeginn,
                    GEnde = e.GEnde,
                    CVonBis = e.CVonBis,
                    HRaum = e.HRaum,
                    IVerantwortlich = e.IVerantwortlich,
                    JKategorie = e.JKategorie,
                    KHinweise = e.KHinweise,
                    LGeschützt = e.LGeschützt,
                    MAnmerkungen = e.MAnmerkungen,
                    NSchuljahr = e.NSchuljahr,
                    ODetails = e.ODetails
                };
                //Add(excelzeile, "unterrichtsfrei");
                
                this.Add(excelzeile);
            }
        }

        private string ZwischenablageKopfzeile()
        {
            string clipboard = "";
            for (int i = 0; i < Global.ColumnNames.Count; i++)
            {
                clipboard += Global.ColumnNames[i] + "\t";
            }

            clipboard = clipboard.Substring(0, clipboard.Length - 1);

            clipboard += Environment.NewLine;
            return clipboard;
        }

        internal void Abschlussverfahren(Excelzeilen excelzeilenQuelle)
        {
            foreach (var e in excelzeilenQuelle.OrderBy(x => x.FBeginn))
            {
                var excelzeile = new Excelzeile()
                {
                    ADatum = e.ADatum,
                    BTag = e.BTag,
                    DBeschreibung = e.DBeschreibung,
                    EJahrgang = e.EJahrgang,
                    FBeginn = e.FBeginn,
                    GEnde = e.GEnde,
                    CVonBis = e.CVonBis,
                    HRaum = e.HRaum,
                    IVerantwortlich = e.IVerantwortlich,
                    JKategorie = e.JKategorie,
                    KHinweise = e.KHinweise,
                    LGeschützt = e.LGeschützt,
                    MAnmerkungen = e.MAnmerkungen
                };
                Add(excelzeile, "FHR Prüfungsplanung");
            }
        }

        internal void Kammerprüfung(Excelzeilen excelzeilenQuelle)
        {
            foreach (var e in excelzeilenQuelle.OrderBy(x => x.FBeginn))
            {
                var excelzeile = new Excelzeile()
                {
                    ADatum = e.ADatum,
                    BTag = e.BTag,
                    DBeschreibung = GetKammerbeschreibung(e.DBeschreibung,e.IVerantwortlich),
                    EJahrgang = e.EJahrgang,
                    FBeginn = e.FBeginn,
                    GEnde = e.GEnde,
                    CVonBis = e.CVonBis,
                    HRaum = e.HRaum,
                    IVerantwortlich = e.IVerantwortlich,
                    JKategorie = e.JKategorie,
                    KHinweise = e.KHinweise,
                    LGeschützt = e.LGeschützt,
                    MAnmerkungen = e.MAnmerkungen
                };

                //Add(excelzeile, "Kammerprüfung");                
                
                this.Add(excelzeile);
            }
        }

        private string GetKammerbeschreibung(string dBeschreibung, List<object> verantwortlich)
        {
            foreach (var item in verantwortlich)
            {
                if (item.ToString().ToLower().Contains("ihk"))
                {
                    return "<img src='https://www.berufskolleg-borken.de/wp-content/uploads/2020/05/ihk-logo.png'> " + dBeschreibung;
                }
                if (item.ToString().ToLower().Contains("hwk"))
                {
                    return "<img src='https://www.berufskolleg-borken.de/wp-content/uploads/2020/05/hwk-logo.png'> " + dBeschreibung;
                }
                if (item.ToString().ToLower().Contains("kh borken"))
                {
                    return "<img src='https://www.berufskolleg-borken.de/wp-content/uploads/2020/05/hwk-logo.png'> " + dBeschreibung;
                }
                if (item.ToString().ToLower().Contains("handwerkerschaft"))
                {
                    return "<img src='https://www.berufskolleg-borken.de/wp-content/uploads/2020/05/hwk-logo.png'> " + dBeschreibung;
                }
                if (item.ToString().ToLower().Contains("lwk"))
                {
                    return "<img src='https://www.berufskolleg-borken.de/wp-content/uploads/2020/05/lwk-logo.png'> " + dBeschreibung;
                }
                if (item.ToString().ToLower().Contains("landwirt"))
                {
                    return "<img src='https://www.berufskolleg-borken.de/wp-content/uploads/2020/05/lwk-logo.png'> " + dBeschreibung;
                }
                if (item.ToString().ToLower().Contains("baugew"))
                {
                    return "<img src='https://www.berufskolleg-borken.de/wp-content/uploads/2020/05/hwk-logo.png'> " + dBeschreibung;
                }
            }
            return dBeschreibung;
        }

        internal void Praktikum(Excelzeilen excelzeilenQuelle, Feriens feriens)
        {
            foreach (var e in excelzeilenQuelle.OrderBy(x => x.FBeginn))
            {
                var excelzeile = new Excelzeile()
                {
                    ADatum = e.ADatum,
                    BTag = e.BTag,
                    DBeschreibung = e.GetPraktikumBeschreibung(feriens),
                    EJahrgang = e.EJahrgang,
                    FBeginn = e.FBeginn,
                    GEnde = e.GEnde,
                    CVonBis = e.CVonBis,
                    HRaum = e.HRaum,
                    IVerantwortlich = e.IVerantwortlich,
                    JKategorie = e.JKategorie,
                    KHinweise = e.KHinweise,
                    LGeschützt = e.LGeschützt,
                    MAnmerkungen = e.MAnmerkungen
                };
                this.Add(excelzeile);
            }            
        }

        internal void Abitur(Excelzeilen excelzeilenQuelle)
        {
            foreach (var e in excelzeilenQuelle.OrderBy(x => x.FBeginn))
            {
                var excelzeile = new Excelzeile()
                {
                    ADatum = e.ADatum,
                    BTag = e.BTag,
                    DBeschreibung = e.DBeschreibung,
                    EJahrgang = e.EJahrgang,
                    FBeginn = e.FBeginn,
                    GEnde = e.GEnde,
                    CVonBis = e.CVonBis,
                    HRaum = e.HRaum,
                    IVerantwortlich = e.IVerantwortlich,
                    JKategorie = e.JKategorie,
                    KHinweise = e.KHinweise,
                    LGeschützt = e.LGeschützt,
                    MAnmerkungen = e.MAnmerkungen
                };             
                this.Add(excelzeile);                
            }
        }


        internal void ToExchange(Lehrers lehrers, Unterrichts unterrichts, Verteilergruppen verteilergruppen)
        {
            Console.Write("Appointments");
            
            ExchangeService service = new ExchangeService(ExchangeVersion.Exchange2013)
            {
                UseDefaultCredentials = true
            };

            service.TraceEnabled = false;
            service.TraceFlags = TraceFlags.All;
            service.Url = new Uri("https://ex01.bkb.local/EWS/Exchange.asmx");

            Excelzeilen e = new Excelzeilen();

            foreach (var lehrer in (from s in this
                                    from k in s.IVerantwortlich                                    
                                    where k.GetType() == typeof(Lehrer)
                                    select ((Lehrer)k)))
            {
                var excelzeilen = new Excelzeilen();

                bool treffer = false;

                foreach (var item in this)
                {
                    foreach (var ver in item.IVerantwortlich)
                    {
                        if (ver.GetType() == typeof(Lehrer)
                              && ((Lehrer)ver).Kürzel == lehrer.Kürzel)
                        {
                            excelzeilen.Add(item);
                            treffer = true;
                        }
                    }
                    foreach (var ver in item.IVerantwortlich)
                    {
                        if (!treffer && ver.GetType() == typeof(Klasse)
                              && ((from u in unterrichts where u.LehrerKürzel == lehrer.Kürzel where ((Klasse)ver).NameUntis == u.KlasseKürzel select u).Any()))
                        {
                            if (!(from ex in excelzeilen where ex.IVerantwortlich == item.IVerantwortlich select ex).Any())
                            {
                                excelzeilen.Add(item);
                                treffer = true;
                            }                            
                        }
                    }                    
                }
                lehrer.ToExchange(excelzeilen, service);
            }
            Console.WriteLine(" ... eingetragen");
        }

        internal void Zeugniskonferenzen(Unterrichts unterrichts, Lehrers lehrers, Excelzeilen excelzeilenQuelle, Raums raums, Klasses klasses, string kat)
        {
            this.ColumnNames = excelzeilenQuelle.ColumnNames;

            for (int i = 0; i < excelzeilenQuelle.Count; i++)
            {
                // Wenn eine Beschreibung existiert, wird sie nicht angerührt.

                string beschreibung = "";

                if (excelzeilenQuelle[i].DBeschreibung != "")
                {
                    beschreibung = excelzeilenQuelle[i].DBeschreibung;
                }
                else
                {
                    Klasse klasse = klasses.FilterKlasse(excelzeilenQuelle[i].IVerantwortlich);
                    var bereichsleitung = (from l in lehrers where l.Kürzel == klasse.Bereichsleitung select l).FirstOrDefault();
                    excelzeilenQuelle[i].IVerantwortlich.Add(bereichsleitung);

                    foreach (var leh in (from u in unterrichts where u.KlasseKürzel == klasse.NameUntis select u.LehrerKürzel).Distinct().ToList())
                    {
                        if (leh != "Raum")
                        {
                            var lehrer = (from l in lehrers where l.Kürzel == leh select l).FirstOrDefault();
                            excelzeilenQuelle[i].IVerantwortlich.Add(lehrer);
                        }                        
                    }

                    var klassenleitung = "";

                    foreach (var kl in klasse.Klassenleitungen)
                    {
                        klassenleitung += RenderAnredeMitMailTo(kl) + " ";
                    }

                    if (bereichsleitung == null)
                    {
                        Console.WriteLine("Die Klasse " + klasse.NameUntis + " hat keine gültige Bereichsleitung");
                        Console.ReadKey();
                    }
                    beschreibung = @"<nobr><b>Zeugniskonferenz " + RenderKlasseMitUrl(klasse) + " </b></nobr><br>Bereichsleitung: " + RenderAnredeMitMailTo(bereichsleitung) + "<br>Klassenleitung: " + klassenleitung.TrimEnd(' ');
                }

                var excelzeile = new Excelzeile()
                {
                    ADatum = excelzeilenQuelle[i].ADatum,
                    BTag = excelzeilenQuelle[i].BTag,
                    DBeschreibung = beschreibung,
                    EJahrgang = excelzeilenQuelle[i].EJahrgang,
                    FBeginn = excelzeilenQuelle[i].FBeginn,
                    GEnde = excelzeilenQuelle[i].GEnde.AddMinutes(10),
                    CVonBis = excelzeilenQuelle[i].CVonBis,
                    HRaum = excelzeilenQuelle[i].HRaum,
                    IVerantwortlich = excelzeilenQuelle[i].IVerantwortlich,
                    JKategorie = excelzeilenQuelle[i].JKategorie,
                    KHinweise = excelzeilenQuelle[i].KHinweise,
                    LGeschützt = excelzeilenQuelle[i].LGeschützt,
                    MAnmerkungen = excelzeilenQuelle[i].MAnmerkungen
                };
                
                this.Add(excelzeile);
            }
        }

        

        private void Add(Excelzeile excelzeile, string kat)
        {
            try
            {
                // Allgemeine Termine werden nach Datum einsortiert

                if (excelzeile.JKategorie.Contains("Allgemein") 
                    || excelzeile.JKategorie.Contains("Kammerprüfung")
                    || excelzeile.JKategorie.Contains("Abitur"))
                {
                    int ii = 0;
                    bool einsortiert = false;
                    foreach (var item in (from e in this
                                          where e.JKategorie.Contains("Allgemein")
                                          select e).ToList())
                    {
                        if (item.FBeginn > excelzeile.FBeginn)
                        {
                            this.Insert(ii, excelzeile);
                            einsortiert = true;
                            break;
                        }
                        ii++;
                    }
                    if (!einsortiert && this.Count > 0)
                    {
                        this.Add(excelzeile);
                    }
                }
                else
                {
                    // Nicht-Allgemeine Termine werden angehangen
                    this.Add(excelzeile);
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        private string RenderKürzelMitMailTo(Lehrer lehrer)
        {
            string anrede = (lehrer.Anrede == "Frau" ? "Frau" : "Herrn") + " " + lehrer.Titel + (lehrer.Titel == "" ? "" : " ") + lehrer.Nachname;

            return "<a title='Nachricht für " + anrede + "' href='mailto: " + lehrer.Mail + " ?subject=Nachricht für " + anrede + "'>" + lehrer.Kürzel + "</a>";
        }

        private string RenderKlasseMitUrl(Klasse k)
        {
            return "<b><a href='" + k.Url + "'>" + k.NameUntis + "</a></b>";
        }

        internal void Vergleiche(Excelzeilen excelzeilenSenke)
        {
            foreach (var item in this)
            {
                int s = (from dd in excelzeilenSenke where dd.DBeschreibung == item.DBeschreibung where dd.ADatum == item.ADatum select dd).Count();
                int q = (from dd in this where dd.DBeschreibung == item.DBeschreibung where dd.ADatum == item.ADatum select dd).Count();

                if (s != 1)
                {
                    Console.WriteLine("In der Senke gibt es den Eintrag " + item.DBeschreibung + "(" + item.ADatum.ToShortDateString() + ") " + q + " mal");
                }

                if (q != 1)
                {
                    Console.WriteLine("In der Quelle gibt es den Eintrag " + item.DBeschreibung + "(" + item.ADatum.ToShortDateString() + ") " + q + " mal");
                }

                if (s != q)
                {
                    Console.WriteLine("Abweichungen von Q u S: " + item.DBeschreibung);
                }
            }
            Console.ReadKey();
        }
                
        internal void ToExcel(Lehrers lehrers, Klasses klasses, Verteilergruppen verteilergruppes, Raums raums, string kategorie)
        {
            Console.Write(kategorie + ".xlsx");

            if (File.Exists(@"c:\\temp\\" + kategorie + ".xlsx"))
            {
                Global.WaitForFile(@"c:\\temp\\" + kategorie + ".xlsx");
                File.Delete(@"c:\\temp\\" + kategorie + ".xlsx");
            }

            Application application = new Application()
            {
                Visible = false,
                DisplayAlerts = false
            };

            var workBook = application.Workbooks.Add(Type.Missing);
            var workSheet = workBook.ActiveSheet;
            workSheet.Name = "Termine";
                        
            try
            {
                workSheet = workBook.Worksheets[1];

                workSheet.Cells[1, 1] = "Datum"; // 28.08.2019 00:00:00
                workSheet.Cells[1, 2] = "Tag";      // Mi 28.08.2019
                workSheet.Cells[1, 3] = "Datum/Zeit"; // Mi 28.08.2019 - Mi 04.09.2019                
                workSheet.Cells[1, 4] = "Beschreibung"; // <b> Maurer/-in </b>
                workSheet.Cells[1, 5] = "Jahrgang"; // Unterstufe
                workSheet.Cells[1, 6] = "Beginn"; // 10:00 
                workSheet.Cells[1, 7] = "Ende";  // 14:00
                workSheet.Cells[1, 8] = "Ort"; // <dfn title='Gebäude 5 | Erdgeschoss'> 5006</dfn>
                workSheet.Cells[1, 9] = "Betrifft"; // <b><nobr><a title='Nachricht für Erweiterte Schulleitung' href='mailto: erweiterte-schulleitung@berufskolleg-borken.de ?subject=Nachricht für Erweiterte Schulleitung'>Erweiterte Schulleitung</a>
                workSheet.Cells[1, 10] = "Kategorie"; 
                workSheet.Cells[1, 11] = "Hinweise"; // 35.-36.KW (2 Wochen)
                workSheet.Cells[1, 12] = "Geschützt"; // <input type='button' target='_blank' value='Informationsabend_D.pptx' 
                workSheet.Cells[1, 13] = "Anmerkungen"; // 10 Wochen vor Schuljahresende

                for (int i = 0; i < this.Count; i++)
                {
                    if ((from t in this[i].JKategorie where t == kategorie select t).Any())
                    {
                        workSheet.Cells[i + 2, 1] = this[i].ADatum;
                        workSheet.Cells[i + 2, 2] = RenderTag(this[i].ADatum);
                        workSheet.Cells[i + 2, 3] = RenderVonBis(this[i].ADatum, this[i].CVonBis, this[i].FBeginn, this[i].GEnde, this[i].JKategorie);
                        // Die Beschreibung wird nicht hier gerendert
                        workSheet.Cells[i + 2, 4] = RenderBeschreibung(this[i].DBeschreibung);
                        workSheet.Cells[i + 2, 5] = this[i].EJahrgang;
                        workSheet.Cells[i + 2, 6] = this[i].FBeginn;
                        workSheet.Cells[i + 2, 7] = this[i].GEnde;
                        workSheet.Cells[i + 2, 8] = RenderRaum(raums, this[i].HRaum);
                        workSheet.Cells[i + 2, 9] = RenderVerantwortliche(lehrers, klasses, verteilergruppes, this[i].IVerantwortlich);
                        workSheet.Cells[i + 2, 10] = RenderKategorie(this[i].JKategorie);
                        workSheet.Cells[i + 2, 11] = RenderHinweis(this[i].KHinweise);
                        workSheet.Cells[i + 2, 12] = this[i].LGeschützt;
                        workSheet.Cells[i + 2, 13] = this[i].MAnmerkungen;
                    }                    
                }
                                
                workBook.SaveAs(@"c:\temp\" + kategorie + ".xlsx");
                workBook.Close();
                application.Quit();
                Marshal.ReleaseComObject(workSheet);
                Marshal.ReleaseComObject(workBook);
                Marshal.ReleaseComObject(application);
                Console.WriteLine(" ... geschrieben.");
                Application excel = new Application();

                ZipAktualisieren(kategorie, "xlsx");

                Process.Start(Global.TerminePfad);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception: " + ex.Message);
                Console.ReadLine();
            }
            finally
            {
                foreach (Process process in Process.GetProcessesByName("Excel"))
                    process.Kill();
            }
        }

        internal void ToJson(Lehrers lehrers, Klasses klasses, Verteilergruppen verteilergruppes, Raums raums, string termine, int id, Optionen optionen, Visibility visibility)
        {
            Console.Write(termine + ".json");

            if (File.Exists(@"c:\\temp\\" + termine + ".json"))
            {
                Global.WaitForFile(@"c:\\temp\\" + termine + ".json");
                File.Delete(@"c:\\temp\\" + termine + ".json");
            }

            List<List<string>> matrix = new List<List<string>>();
            
            try
            {
                List<string> zeile = new List<string>
                {
                    "Datum",
                    "Tag",
                    "Datum/Zeit",
                    "Beschreibung",
                    "Jahrgang",
                    "Beginn",
                    "Ende",
                    "Ort",
                    "Betrifft",
                    "Kategorie",
                    "Hinweise",
                    "Geschützt",
                    "Anmerkungen"
                };
                matrix.Add(zeile);

                for (int i = 0; i < this.Count; i++)
                {
                    zeile = new List<string>();
                    zeile.Add(this[i].ADatum.ToString("dd.MM.yyyy HH:mm:ss"));
                    zeile.Add(RenderTag(this[i].ADatum));
                    zeile.Add(RenderVonBis(this[i].ADatum, this[i].CVonBis, this[i].FBeginn, this[i].GEnde, this[i].JKategorie));
                    zeile.Add(RenderBeschreibung(this[i].DBeschreibung));
                    zeile.Add(this[i].EJahrgang);
                    zeile.Add(this[i].FBeginn.ToString("dd.MM.yyyy HH:mm:ss"));
                    zeile.Add(this[i].GEnde.ToString("dd.MM.yyyy HH:mm:ss"));
                    zeile.Add(RenderRaum(raums, this[i].HRaum));
                    zeile.Add(RenderVerantwortliche(lehrers, klasses, verteilergruppes, this[i].IVerantwortlich));
                    zeile.Add(RenderKategorie(this[i].JKategorie));
                    zeile.Add(RenderHinweis(this[i].KHinweise));
                    zeile.Add(this[i].LGeschützt);
                    zeile.Add(this[i].MAnmerkungen);

                    matrix.Add(zeile);
                }
                                
                Jon json = new Jon()
                {
                    id = id.ToString(),
                    name = termine,
                    description = termine,
                    author = "11",
                    last_modified = "2020-04-26 13:55:30",
                    data = matrix,
                    options = optionen, 
                    visibility = visibility
                };

                // string jjson = JsonConvert.SerializeObject(json);

                File.WriteAllText(@"c:\temp\" + termine + ".json", JsonConvert.SerializeObject(json));
                
                using (StreamWriter file = File.CreateText(@"c:\temp\" + termine + ".json"))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    serializer.Serialize(file, json);
                }
                
                ZipAktualisieren(termine, "json");

                Process.Start(Global.TerminePfad);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception: " + ex.Message);
                Console.ReadLine();
            }
            finally
            {
                foreach (Process process in Process.GetProcessesByName("Excel"))
                    process.Kill();
            }
        }

        public static void ZipAktualisieren(string termine, string typ)
        {
            Console.Write("Zipdatei");
            
            if (!File.Exists(Global.TerminePfad + "Termine-" + typ + ".zip"))
            {
                using (FileStream zipToOpen = new FileStream(Global.TerminePfad + "Termine-" + typ + ".zip", FileMode.CreateNew))
                {
                    // Readme wird hinzugefügt

                    //using (ZipArchive archive = new ZipArchive(zipToOpen, ZipArchiveMode.Update))
                    //{
                    //    Console.Write(" ... erstellt");
                    //    ZipArchiveEntry readmeEntry = archive.CreateEntry("Readme.txt");
                    //    using (StreamWriter writer = new StreamWriter(readmeEntry.Open()))
                    //    {
                    //        writer.WriteLine("TablePress");
                    //        writer.WriteLine("==========");
                    //    }
                    //}
                }
            }

            // Zuerst die alte Datei löschen, falls existent
            try
            {
                using (var zipArchive = ZipFile.Open(Global.TerminePfad + "Termine-" + typ + ".zip", ZipArchiveMode.Update))
                {
                    ZipArchiveEntry entry = zipArchive.GetEntry(termine + "." + typ);
                    entry.Delete();
                    Console.Write(" ... " + termine + " gelöscht");
                }
            }
            catch (Exception)
            {
            }            

            // Datei wird zur Zip hinzugefügt:

            using (var zipArchive = ZipFile.Open(Global.TerminePfad + "Termine-" + typ + ".zip", ZipArchiveMode.Update))
            {
                var fileInfo = new FileInfo(@"c:\\temp\\" + termine + "." + typ);
                zipArchive.CreateEntryFromFile(fileInfo.FullName, fileInfo.Name);
                Console.WriteLine(" ... " + termine + ". " + typ + " hinzugefügt.");
            }
        }

        public Excelzeilen()
        {
        }

        public Excelzeilen(Klasses klasses)
        {
            this.Klasses = klasses;
        }

        internal void Allgemein(Excelzeilen excelzeilenQuelle)
        {
            this.ColumnNames = excelzeilenQuelle.ColumnNames;

            Excelzeilen e = new Excelzeilen();

            for (int i = 0; i < excelzeilenQuelle.Count; i++)
            {   
                var excelzeile = new Excelzeile()
                {
                    ADatum = excelzeilenQuelle[i].ADatum,
                    BTag = excelzeilenQuelle[i].BTag,
                    DBeschreibung = excelzeilenQuelle[i].DBeschreibung,
                    EJahrgang = excelzeilenQuelle[i].EJahrgang,
                    FBeginn = excelzeilenQuelle[i].FBeginn,
                    GEnde = excelzeilenQuelle[i].GEnde,
                    CVonBis = excelzeilenQuelle[i].CVonBis,
                    HRaum = excelzeilenQuelle[i].HRaum,
                    IVerantwortlich = excelzeilenQuelle[i].IVerantwortlich,
                    JKategorie = excelzeilenQuelle[i].JKategorie,
                    KHinweise = excelzeilenQuelle[i].KHinweise,
                    LGeschützt = excelzeilenQuelle[i].LGeschützt,
                    MAnmerkungen = excelzeilenQuelle[i].MAnmerkungen
                };
                                        
                e.Add(excelzeile);                  
            }
            
            // Sortieren

            Excelzeilen xx = new Excelzeilen();

            foreach (var item in (from se in e select se).OrderBy(cc => cc.ADatum).ThenBy(ccc => ccc.FBeginn))
            {
                xx.Add(item);                
            }
            this.AddRange(xx);            
        }

        private string RenderVonBis(DateTime aDatum, List<DateTime> vonBis, DateTime fBeginn, DateTime gEnde, List<string> kategorie)
        {
            string v = "";

            if ((from k in kategorie where k == "unbestätigt" select k).Any())
            {
                return "N/A";
            }
            
            if (vonBis == null || vonBis.Count == 0)
            {
                v += "";                                
            }
            else
            {
                // Wenn Beginn und Ende am selben Tag sind

                if (fBeginn.Date == gEnde.Date)
                {                    
                    v += string.Format("{0:ddd}", fBeginn) + " " + fBeginn.ToString("dd.MM.yyyy") + " " ;

                    if(fBeginn.Hour != 0)
                    {
                        v += "</br>" + fBeginn.ToString("HH:mm") + "&nbsp;Uhr";
                    }
                    if (gEnde.Hour != 0)
                    {
                        v += "-" + gEnde.ToString("HH:mm") + "&nbsp;Uhr";
                    }
                }
                else
                {
                    v += "<nobr>" + string.Format("{0:ddd}", vonBis[0]) + " " + vonBis[0].ToString("dd.MM.yyyy") + " - </nobr><br><nobr>" + string.Format("{0:ddd}", vonBis[1]) + " " + vonBis[1].ToString("dd.MM.yyyy") + "</nobr>";
                }
            }            
            
            return v;
        }

        internal void ToZwischenablage(Lehrers lehrers, Klasses klasses, Verteilergruppen verteilergruppen, Raums raums, Feriens feriens)
        {
            try
            {
                // Alle relevanten Termine heraussuchen

                Excelzeilen relevant = new Excelzeilen();

                relevant.AddRange((from x in this
                                   where (x.ADatum.Date >= DateTime.Now.Date /*&& x.ADatum.Date < Global.LetzterTagDesSchuljahres.AddDays(20)*/) ||
                                   x.JKategorie.Contains("ErsterSchultag") ||
                                   x.JKategorie.Contains("Berufsschultage") ||
                                   x.JKategorie.Contains("unterrichtsfrei") ||
                                   x.JKategorie.Contains("Blockzeit")
                                   select x).ToList());

                int z = 0;                
                int i = 0;
                
                for (i = 0; i < relevant.Count; i++)
                {
                    Global.Zwischenablage += relevant[i].ADatum + "\t";
                    Global.Zwischenablage += RenderTag(relevant[i].ADatum) + "\t";
                    Global.Zwischenablage += RenderVonBis(relevant[i].ADatum, relevant[i].CVonBis, relevant[i].FBeginn, relevant[i].GEnde, relevant[i].JKategorie) + "\t";
                    Global.Zwischenablage += RenderBeschreibung(relevant[i].DBeschreibung) + "\t";
                    Global.Zwischenablage += relevant[i].EJahrgang + "\t";
                    Global.Zwischenablage += relevant[i].FBeginn + "\t";
                    Global.Zwischenablage += relevant[i].GEnde + "\t";
                    Global.Zwischenablage += RenderRaum(raums, relevant[i].HRaum) + "\t";
                    Global.Zwischenablage += RenderVerantwortliche(lehrers, klasses, verteilergruppen, relevant[i].IVerantwortlich) + "\t";
                    Global.Zwischenablage += RenderKategorie(relevant[i].JKategorie) + "\t";                    
                    Global.Zwischenablage += RenderHinweis(relevant[i].KHinweise) + "\t";
                    Global.Zwischenablage += relevant[i].LGeschützt + "\t";
                    Global.Zwischenablage += relevant[i].MAnmerkungen + "\t";
                    Global.Zwischenablage += relevant[i].NSchuljahr + "\t";
                    Global.Zwischenablage += relevant[i].ODetails + "\t";
                    Global.Zwischenablage += Environment.NewLine;
                    z++;
                }

                // Für fehlende Kategorien wird ein Fehleintrag erstellt.

                foreach (var kat in new List<string>()
                {
                    "Allgemein",
                    "Kammerprüfung",
                    "Zeugniskonferenz HZBC",
                    "Zeugniskonferenz HZD",
                    "Zeugniskonferenz JZBC",
                    "Zeugniskonferenz JZD",
                    "unterrichtsfrei",
                    "Abitur",
                    "Sprechtag",
                    "ErsterSchultag",
                    "Praktikum",
                    "Anmeldung",
                    "Voreinschulung",
                    "Blockzeit",
                    "Prüfungen",
                    "FHR Prüfungsplanung",
                    "ZulassungskonferenzBC"
                })
                {
                    if (!(from x in relevant from zz in x.JKategorie where zz.Contains(kat) select x).Any())
                    {
                        Global.Zwischenablage += "\t";
                        Global.Zwischenablage += "\t";
                        Global.Zwischenablage += "\t";
                        Global.Zwischenablage += "Zur Zeit nichts anzuzeigen." + "\t";
                        Global.Zwischenablage += "\t";
                        Global.Zwischenablage += "\t";
                        Global.Zwischenablage += "\t";
                        Global.Zwischenablage += "\t";
                        Global.Zwischenablage += "\t";
                        Global.Zwischenablage += kat  + "\t";
                        Global.Zwischenablage += "\t";
                        Global.Zwischenablage += "\t";
                        Global.Zwischenablage += "\t";
                        Global.Zwischenablage += Environment.NewLine;
                        z++;
                    }
                }

                Console.WriteLine(" ... " + z + " Zeilen in die Zwischenablage geschrieben");

                if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\Zwischenablage.txt"))
                {
                    File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\Zwischenablage.txt");
                }

                using (StreamWriter outputFile = new StreamWriter(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\Zwischenablage.txt"))
                {
                    outputFile.Write(Global.Zwischenablage);
                }

                string url = "https://www.berufskolleg-borken.de/wp-admin/admin.php?page=tablepress_import";
                System.Diagnostics.Process.Start(url);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Console.ReadKey();
            }
        }
        
        private string RenderBeschreibung(string dBeschreibung)
        {
            // Landeswappen bei Zentralprüfungen

            if (dBeschreibung.StartsWith("<b>Zentral"))
            {
                return "<img src='https://www.berufskolleg-borken.de/wp-content/uploads/2020/04/nrw-wappen.png'> " + dBeschreibung;
            }
            return dBeschreibung;
        }

        private string RenderBody(string beschreibung, string hinweise)
        {
            return beschreibung + "</br>" + hinweise;
        }

        private string RenderSubject(string beschreibung)
        {
            return beschreibung.Replace("</br>", " - ").Replace("<b>", "").Replace("</b>","").Trim();
        }

        private dynamic RenderHinweis(string kHinweise)
        {
            return kHinweise.Replace("Beweglicher Ferientag", "<b><a href='https://www.berufskolleg-borken.de/beweglicheFerientage/'>Beweglicher Ferientag</a></b>");
        }

        private int GetKalenderwoche(DateTime datum)
        {
            CultureInfo CUI = CultureInfo.CurrentCulture;
            return CUI.Calendar.GetWeekOfYear(datum, CUI.DateTimeFormat.CalendarWeekRule, CUI.DateTimeFormat.FirstDayOfWeek);
        }

        private dynamic RenderKategorie(List<string> jKategorie)
        {
            var x = "";

            foreach (var item in jKategorie)
            {
                x += item + ",";
            }
            return x.TrimEnd(',');
        }

        private string RenderVerantwortliche(Lehrers lehrers, Klasses klasses, Verteilergruppen verteilergruppen, List<object> verantwortlich)
        {            
            var x = "";

            foreach (var item in verantwortlich)
            {
                if(item.GetType() == typeof(Lehrer))
                {
                    string url = "https://www.berufskolleg-borken.de/das-kollegium/#Bild";

                    // Wenn der Lehrende nicht in einer Verteilergruppe ist, 

                    if ((from v in verteilergruppen where v.Name == ((Lehrer)item).Kürzel select v).Any())
                    {
                        url = (from v in verteilergruppen where v.Name == ((Lehrer)item).Kürzel select v.Url).FirstOrDefault();
                    }
                                        
                    x += RenderKürzelMitMailTo(((Lehrer)item)) + " ";
                }
                if (item.GetType() == typeof(Klasse))
                {
                    x += RenderKlasseMitUrl((Klasse)item) + "<br>";
                }
                if (item.GetType() == typeof(Verteilergruppe))
                {
                    if (((Verteilergruppe)item).Mail != "")
                    {
                        x += " <b><nobr><a title='Nachricht für " + ((Verteilergruppe)item).Name + "' href='mailto: " + ((Verteilergruppe)item).Mail + " ?subject=Nachricht für " + ((Verteilergruppe)item).Name + "'>" + ((Verteilergruppe)item).Name + "</a></nobr><br> ";
                    }
                    else
                    {
                        x += "<b><a target='_blank' rel='noopener noreferrer' href='" + ((Verteilergruppe)item).Url + "'>" + ((Verteilergruppe)item).Name + "</a></b> ";
                    }                    
                }
            }
            return x.TrimEnd(' ');
        }

        private string RenderTag(DateTime aDatum)
        {
            return string.Format("{0:ddd}", aDatum) + " " + aDatum.ToString("dd.MM.yyyy");
        }

        private string RenderRaum(Raums raums, Raums hRaums)
        {
            try
            {
                string r = "";

                if (hRaums.Count > 0)
                {
                    if (hRaums != null && hRaums.Count > 0 && hRaums[0].Raumnummer == "Forum")
                    {
                        string aa = "";
                    }
                    foreach (var raum in (from h in hRaums select h.Raumnummer).Distinct())
                    {                        
                        r += "<dfn title='";
                        
                        if ((from rr in raums where rr.Raumnummer == raum select r).Any() && int.TryParse(raum, out int x))
                        {                           
                            r += "Gebäude " + raum.ToString().Substring(0, 1) + " | " + (raum.ToString().Substring(1, 1) == "0" ? "Erdgeschoss" : raum.ToString().Substring(1, 1) + ". Etage");
                        }
                        else
                        {

                        }

                        r += "'> " + raum + "</dfn>";
                    }
                }

                if (r == "" && hRaums.Count > 0)
                {
                    return hRaums[0].Raumname;
                }
                return r;
            }
            catch (Exception)
            {                
                return "";
            }
        }
        
        private string RenderAnredeMitMailTo(Lehrer lehrer)
        {
            try
            {
                string anrede = (lehrer.Anrede == "Frau" ? "Frau" : "Herrn") + " " + lehrer.Titel + (lehrer.Titel == "" ? "" : " ") + lehrer.Nachname;

                return "<b><nobr><a title='Nachricht für " + anrede + "' href='mailto: " + lehrer.Mail + " ?subject=Nachricht für " + anrede + "'>" + lehrer.Anrede + (lehrer.Titel == "" ? "" : " " + lehrer.Titel) + " " + lehrer.Nachname + "</a></b></nobr>";
            }
            catch (Exception ex)
            {

                throw ex;
            }            
        }
    }
}