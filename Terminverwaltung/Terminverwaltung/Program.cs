﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;

namespace Terminverwaltung
{
    class Program
    {   public static List<string> Block = new List<string>() { "BU", "BM", "BO", "EIU", "EIM", "EIO", "HTU", "HTM", "HTO", "AGGU", "AGGM", "AGGO", "MM", "MU" };
        public static Feriens feriens;
        public static Periodes periodes;
        public static DateTime ersterSchultag;
        public static Fachs fachs;
        public static Unterrichtsgruppes unterrichtsgruppes;
        public static Raums raums;
        public static Lehrers lehrers;
        public static Klasses klasses;
        public static Anrechnungs anrechnungs;
        public static Unterrichts unterrichts;
        public static Schuelers schuelers;
        public static Verteilergruppen verteilergruppen;
        public static Excelzeilen excelzeilenQuelle;
        public static Excelzeilen excelzeilenSenke;
        public static Adressen adressen;
        public static Betriebe betriebe;

        [STAThread]
        static void Main(string[] args)
        {
            System.Net.ServicePointManager.ServerCertificateValidationCallback = ((sender, certificate, chain, sslPolicyErrors) => true);

            int sj = (DateTime.Now.Month >= 8 ? DateTime.Now.Year : DateTime.Now.Year - 1);
            Global.AktSjAtl = sj.ToString() + "/" + (sj + 1 - 2000);

            sj = (DateTime.Now.Month >= 8 ? DateTime.Now.Year : DateTime.Now.Year - 1);
            Global.AktSjUnt = sj.ToString() + (sj + 1);

            try
            {
                periodes = periodes ?? new Periodes();
                feriens = feriens ?? new Feriens();
                ersterSchultag = GetErsterSchultag(periodes, feriens);
                fachs = fachs ?? new Fachs();
                raums = new Raums(periodes);
                lehrers = new Lehrers(raums, periodes);
                klasses = new Klasses(lehrers, raums, periodes);                
                verteilergruppen = new Verteilergruppen();
                unterrichtsgruppes = new Unterrichtsgruppes();
                anrechnungs = new Anrechnungs(periodes);
                unterrichts = new Unterrichts(periodes.AktuellePeriode, klasses, lehrers, fachs, raums, unterrichtsgruppes);
                klasses.ErsterSchultag(unterrichts, ersterSchultag, Block);
                adressen = new Adressen();
                betriebe = new Betriebe();
                                
                MainMenu();                
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Console.ReadKey();
            }
        }

        private static bool MainMenu()
        {
            Global.Zwischenablage = "";
            Console.Clear();
            
            excelzeilenSenke = new Excelzeilen(klasses);
            
            Console.WriteLine("Terminplanung (Version 20200512)");
            Console.WriteLine("================================");
            Console.WriteLine("");
            Console.WriteLine("Welche Termine sollen geplant werden?");            
            Console.WriteLine(" A) Allgemeine Termine");
            Console.WriteLine(" Z) Zeugniskonferenzen");
            Console.WriteLine(" S) Sprechtag");
            Console.WriteLine(" K) Kammerprüfungen");
            Console.WriteLine(" L) Zulassungskonferenzen");
            Console.WriteLine(" P) Praktika");
            Console.WriteLine(" F) FHR-Prüfungen");
            Console.WriteLine(" E) Erster Schultag");
            Console.WriteLine(" U) Unterrichtsfrei / Bewegliche Ferientage");
            Console.WriteLine(" B) Blockzeiten");
            Console.WriteLine(" H) Abitur");
            Console.WriteLine(" V) Voreinschulung");
            Console.WriteLine("ENTER) Alle");
            Console.WriteLine("X) Exit");
            Console.Write("\r\nIhre Auswahl: ");

            ConsoleKeyInfo k = new ConsoleKeyInfo();
            
            Console.WriteLine("Treffen Sie in den nächsten 10 Sekunden eine Auswahl.");
            for (int cnt = 10; cnt > 0; cnt--)
            {
                if (Console.KeyAvailable == true)
                {
                    k = Console.ReadKey();
                    break;
                }
                else
                {
                    Console.WriteLine(cnt.ToString());
                    System.Threading.Thread.Sleep(1000);
                }
            }
            Console.WriteLine("Auswahl: " + k.Key);
            
            if (k.Key == ConsoleKey.A || k.Key == ConsoleKey.Enter || k.Key == 0) // Allgemein
            {
                string kategorie = "Allgemein";
                excelzeilenQuelle = new Excelzeilen(kategorie, lehrers, klasses, verteilergruppen, raums, ersterSchultag);
                excelzeilenSenke.Allgemein(excelzeilenQuelle);
            }

            if (k.Key == ConsoleKey.K || k.Key == ConsoleKey.Enter || k.Key == 0)  // Kammerprüfung
            {
                string kategorie = "Kammerprüfung";
                excelzeilenQuelle = new Excelzeilen(kategorie, lehrers, klasses, verteilergruppen, raums, ersterSchultag);
                excelzeilenSenke.Kammerprüfung(excelzeilenQuelle);
            }

            if (k.Key == ConsoleKey.U || k.Key == ConsoleKey.Enter || k.Key == 0)  // unterrichtsfrei
            {
                string kategorie = "unterrichtsfrei";
                excelzeilenQuelle = new Excelzeilen(kategorie, lehrers, klasses, verteilergruppen, raums, ersterSchultag);
                excelzeilenSenke.Unterrichtsfrei(excelzeilenQuelle, feriens);
            }

            if (k.Key == ConsoleKey.H || k.Key == ConsoleKey.Enter || k.Key == 0) // Abitur
            {
                string kategorie = "Abitur";
                excelzeilenQuelle = new Excelzeilen(kategorie, lehrers, klasses, verteilergruppen, raums, ersterSchultag);
                excelzeilenSenke.Abitur(excelzeilenQuelle);
            }

            if (k.Key == ConsoleKey.P || k.Key == ConsoleKey.Enter || k.Key == 0)  // Praktikum
            {
                string kategorie = "Praktikum";
                excelzeilenQuelle = new Excelzeilen(kategorie, lehrers, klasses, verteilergruppen, raums, ersterSchultag);
                excelzeilenSenke.Praktikum(excelzeilenQuelle, feriens);
            }

            if (k.Key == ConsoleKey.F || k.Key == ConsoleKey.Enter || k.Key == 0)  // FHR Prüfungsplanung
            {
                string kategorie = "FHR-Prüfungsplanung";
                excelzeilenQuelle = new Excelzeilen(kategorie, lehrers, klasses, verteilergruppen, raums, ersterSchultag);
                excelzeilenSenke.PrüfungsplanungFHR(excelzeilenQuelle);
            }

            if (k.Key == ConsoleKey.Z || k.Key == ConsoleKey.Enter || k.Key == 0)  // Zeugniskonferenzen
            {
                foreach (var konf in new List<string>() { "HZBC", "HZD", "JZBC", "JZD" })
                {
                    string kategorie = "Zeugniskonferenz";
                    excelzeilenQuelle = new Excelzeilen(kategorie + " " + konf, lehrers, klasses, verteilergruppen, raums, ersterSchultag);
                    excelzeilenSenke.Zeugniskonferenzen(unterrichts, lehrers, excelzeilenQuelle, raums, klasses, konf);
                }
            }

            // Die Liste wird nach Datum sortiert

            excelzeilenSenke.Sort((x, y) => x.FBeginn.CompareTo(y.FBeginn));

            // Ab hier wird die Reihenfolge durch die Exceldatei bestimmt.

            if (k.Key == ConsoleKey.V || k.Key == ConsoleKey.Enter || k.Key == 0)  // Voreinschulung
            {
                string kategorie = "Voreinschulung";
                excelzeilenQuelle = new Excelzeilen(kategorie, lehrers, klasses, verteilergruppen, raums, ersterSchultag);
                excelzeilenSenke.Voreinschulung(excelzeilenQuelle, lehrers, klasses);
            }

            if (k.Key == ConsoleKey.S || k.Key == ConsoleKey.Enter || k.Key == 0)  // Sprechtag
            {
                var kategorie = "Sprechtag";
                excelzeilenQuelle = new Excelzeilen(kategorie, lehrers, klasses, verteilergruppen, raums, ersterSchultag);
                excelzeilenSenke.Sprechtag(lehrers, unterrichts, raums, klasses, verteilergruppen, excelzeilenQuelle, anrechnungs);
            }
            
            if (k.Key == ConsoleKey.L || k.Key == ConsoleKey.Enter || k.Key == 0)  // Zulassungskonferenz BC
            {
                string kategorie = "ZulassungskonferenzBC";
                excelzeilenQuelle = new Excelzeilen(kategorie, lehrers, klasses, verteilergruppen, raums, ersterSchultag);

                // Notenlisten werden nur gedruckt, wenn "L" gedrückt wurde

                if (!(k.Key == ConsoleKey.Enter || k.Key == 0))
                {
                    ExportLessons exportLessons = new ExportLessons();
                    StudentgroupStudents studentgroupStudents = new StudentgroupStudents(exportLessons);
                    Noten noten = new Noten();
                    Sortierung sortierung = new Sortierung();
                    schuelers = new Schuelers(betriebe, adressen, klasses);
                    schuelers.FächerUndNoten(fachs, exportLessons, studentgroupStudents, noten, sortierung);
                    klasses.Notenlisten(schuelers, lehrers, k.Key);
                }

                excelzeilenSenke.Zulassungskonferenzen(excelzeilenQuelle, klasses, lehrers);
            }

            if (k.Key == ConsoleKey.B || k.Key == ConsoleKey.Enter || k.Key == 0)  // Blockzeit
            {
                string kategorie = "Blockzeit";
                excelzeilenQuelle = new Excelzeilen(kategorie, lehrers, klasses, verteilergruppen, raums, ersterSchultag);
                excelzeilenSenke.Blockzeit(klasses, excelzeilenQuelle);
            }

            if (k.Key == ConsoleKey.E || k.Key == ConsoleKey.Enter || k.Key == 0)  // ErsterSchultag
            {
                // Nach April 
                ersterSchultag = new DateTime(2020, 08, 12);
                string kategorie = "ErsterSchultag";

                // Nach April wird das neue SJ geladen
                int sj = (DateTime.Now.Month >= 5 ? DateTime.Now.Year : DateTime.Now.Year - 1);
                Global.AktSjAtl = sj.ToString() + "/" + (sj + 1 - 2000);

                sj = (DateTime.Now.Month >= 5 ? DateTime.Now.Year : DateTime.Now.Year - 1);
                Global.AktSjUnt = sj.ToString() + (sj + 1);

                periodes = new Periodes();                
                fachs = fachs ?? new Fachs();
                raums = new Raums(periodes);
                lehrers = new Lehrers(raums, periodes);
                klasses = new Klasses(lehrers, raums, periodes);
                verteilergruppen = new Verteilergruppen();
                unterrichtsgruppes = new Unterrichtsgruppes();
                anrechnungs = new Anrechnungs(periodes);
                unterrichts = new Unterrichts(periodes.AktuellePeriode, klasses, lehrers, fachs, raums, unterrichtsgruppes);
                klasses.ErsterSchultag(unterrichts, ersterSchultag, Block);
                
                excelzeilenQuelle = new Excelzeilen(kategorie, lehrers, klasses, verteilergruppen, raums, ersterSchultag);
                klasses.ErsterSchultag(unterrichts, ersterSchultag, Block);
                excelzeilenSenke.Add(klasses.ErsterSchultagNeueVollzeitklassen(ersterSchultag));
                excelzeilenSenke.Add(klasses.ErsterSchultagFortgeführteVollzeitklassen(ersterSchultag));
                excelzeilenSenke.AddRange(klasses.Berufsschultage(unterrichts, unterrichtsgruppes, ersterSchultag));               
            }

            excelzeilenSenke.RenderHtml();
            
            excelzeilenSenke.ToZwischenablage(lehrers, klasses, verteilergruppen, raums, feriens);

            //excelzeilenSenke.ToIcs(lehrers, klasses, verteilergruppen, raums, feriens);

            Process myProcess = new Process();
            Process.Start("notepad++.exe", Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\Zwischenablage.txt");
            
            System.Windows.Forms.Clipboard.SetText(Global.Zwischenablage);            
            return true;
        }
        
        private static string CaptureInput()
        {
            Console.Write("Enter the string you want to modify: ");
            return Console.ReadLine();
        }

        private static void ReverseString()
        {
            Console.Clear();
            Console.WriteLine("Reverse String");

            char[] charArray = CaptureInput().ToCharArray();
            Array.Reverse(charArray);
            DisplayResult(String.Concat(charArray));
        }

        private static void RemoveWhitespace()
        {
            Console.Clear();
            Console.WriteLine("Remove Whitespace");

            DisplayResult(CaptureInput().Replace(" ", ""));
        }

        private static void DisplayResult(string message)
        {
            Console.WriteLine($"\r\nYour modified string is: {message}");
            Console.Write("\r\nPress Enter to return to Main Menu");
            Console.ReadLine();
        }

        private static DateTime GetErsterSchultag(Periodes periodes, Feriens feriens)
        {
            var ersterTagErstePeriode = (from p in periodes where p.IdUntis == 1 select p.Von).FirstOrDefault();

            foreach (var ferien in feriens)
            {
                if (ersterTagErstePeriode.ToShortDateString() == ferien.Von.ToShortDateString())
                {
                    ersterTagErstePeriode = ersterTagErstePeriode.AddDays(1);
                }
            }
            return ersterTagErstePeriode;
        }
    }
}