﻿using System;

namespace Terminverwaltung
{
    public class Termin
    {
        public string Name { get; set; }
        public DateTime DatumUhrzeit { get; set; }
        public Lehrers BeteiligteLehrer { get; set; }
                
        public Termin()
        {
        }
    }
}