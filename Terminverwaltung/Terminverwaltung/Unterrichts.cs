﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;

namespace Terminverwaltung
{
    public class Unterrichts : List<Unterricht>
    {
        public Lehrers Lehrers { get; set; }
        public DateTime Schuljahresbeginn { get; private set; }

        public Unterrichts()
        {
        }

        public Unterrichts(int periode, Klasses klasses, Lehrers lehrers, Fachs fachs, Raums raums, Unterrichtsgruppes unterrichtsgruppes)
        {
            Lehrers = lehrers;

            using (OleDbConnection oleDbConnection = new OleDbConnection(Global.ConU))
            {
                int id = 0;

                try
                {
                    string queryString = @"SELECT DISTINCT 
Lesson_ID,
LessonElement1,
Periods,
Lesson.LESSON_GROUP_ID,
Lesson_TT,
Flags,
DateFrom,
DateTo
FROM LESSON
WHERE (((SCHOOLYEAR_ID)= " + Global.AktSjUnt + ") AND ((TERM_ID)=" + periode + ") AND ((Lesson.SCHOOL_ID)=177659) AND (((Lesson.Deleted)=No))) ORDER BY LESSON_ID;";

                    OleDbCommand oleDbCommand = new OleDbCommand(queryString, oleDbConnection);
                    oleDbConnection.Open();
                    OleDbDataReader oleDbDataReader = oleDbCommand.ExecuteReader();

                    Console.WriteLine("Unterrichte");
                    Console.WriteLine("-----------");

                    while (oleDbDataReader.Read())
                    {
                        id = oleDbDataReader.GetInt32(0);
                        
                        string wannUndWo = Global.SafeGetString(oleDbDataReader, 4);

                        var zur = wannUndWo.Replace("~~", "|").Split('|');

                        ZeitUndOrts zeitUndOrts = new ZeitUndOrts();

                        for (int i = 0; i < zur.Length; i++)
                        {
                            if (zur[i] != "")
                            {
                                var zurr = zur[i].Split('~');

                                int tag = 0;
                                int stunde = 0;
                                List<string> raum = new List<string>();

                                try
                                {
                                    tag = Convert.ToInt32(zurr[1]);
                                }
                                catch (Exception)
                                {
                                    Console.WriteLine("Der Unterricht " + id + " hat keinen Tag.");
                                }

                                try
                                {
                                    stunde = Convert.ToInt32(zurr[2]);
                                }
                                catch (Exception)
                                {
                                    Console.WriteLine("Der Unterricht " + id + " hat keine Stunde.");
                                }

                                try
                                {
                                    var ra = zurr[3].Split(';');

                                    foreach (var item in ra)
                                    {
                                        if (item != "")
                                        {
                                            raum.AddRange((from r in raums
                                                           where item.Replace(";", "") == r.IdUntis.ToString()
                                                           select r.Raumnummer));
                                        }
                                    }

                                    if (raum.Count == 0)
                                    {
                                        raum.Add("");
                                    }
                                }
                                catch (Exception)
                                {
                                    Console.WriteLine("Der Unterricht " + id + " hat keinen Raum.");
                                }


                                ZeitUndOrt zeitUndOrt = new ZeitUndOrt(tag, stunde, raum);
                                zeitUndOrts.Add(zeitUndOrt);
                            }
                        }

                        string lessonElement = Global.SafeGetString(oleDbDataReader, 1);

                        int anzahlGekoppelterLehrer = lessonElement.Count(x => x == '~') / 21;

                        List<string> klassenKürzel = new List<string>();

                        for (int i = 0; i < anzahlGekoppelterLehrer; i++)
                        {
                            var lesson = lessonElement.Split(',');

                            var les = lesson[i].Split('~');
                            string lehrer = les[0] == "" ? null : (from l in lehrers where l.IdUntis.ToString() == les[0] select l.Kürzel).FirstOrDefault();

                            string fach = les[2] == "0" ? "" : (from f in fachs where f.IdUntis.ToString() == les[2] select f.KürzelUntis).FirstOrDefault();

                            string raumDiesesUnterrichts = "";
                            if (les[3] != "")
                            {
                                raumDiesesUnterrichts = (from r in raums where (les[3].Split(';')).Contains(r.IdUntis.ToString()) select r.Raumnummer).FirstOrDefault();
                            }

                            int anzahlStunden = oleDbDataReader.GetInt32(2);

                            var unterrichtsgruppeDiesesUnterrichts = (from u in unterrichtsgruppes where u.IdUntis == oleDbDataReader.GetInt32(3) select u).FirstOrDefault();

                            if (les.Count() >= 17)
                            {
                                foreach (var kla in les[17].Split(';'))
                                {
                                    Klasse klasse = new Klasse();

                                    if (kla != "")
                                    {
                                        if (!(from kl in klassenKürzel
                                              where kl == (from k in klasses
                                                           where k.IdUntis == Convert.ToInt32(kla)
                                                           select k.NameUntis).FirstOrDefault()
                                              select kl).Any())
                                        {
                                            klassenKürzel.Add((from k in klasses
                                                               where k.IdUntis == Convert.ToInt32(kla)
                                                               select k.NameUntis).FirstOrDefault());
                                        }
                                    }
                                }
                            }
                            else
                            {
                            }

                            if (lehrer != null)
                            {
                                for (int z = 0; z < zeitUndOrts.Count; z++)
                                {
                                    // Wenn zwei Lehrer gekoppelt sind und zwei Räume zu dieser Stunde gehören, dann werden die Räume entsprechend verteilt.

                                    string r = zeitUndOrts[z].Raum[0];
                                    try
                                    {
                                        if (anzahlGekoppelterLehrer > 1 && zeitUndOrts[z].Raum.Count > 1)
                                        {
                                            r = zeitUndOrts[z].Raum[i];
                                        }
                                    }
                                    catch (Exception)
                                    {
                                        if (anzahlGekoppelterLehrer > 1 && zeitUndOrts[z].Raum.Count > 1)
                                        {
                                            r = zeitUndOrts[z].Raum[0];
                                        }
                                    }

                                    string k = "";

                                    foreach (var item in klassenKürzel)
                                    {
                                        k += item + ",";
                                    }

                                    // Nur wenn der tagDesUnterrichts innerhalb der Befristung stattfindet, wird er angelegt

                                    DateTime von = DateTime.ParseExact((oleDbDataReader.GetInt32(6)).ToString(), "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
                                    DateTime bis = DateTime.ParseExact((oleDbDataReader.GetInt32(7)).ToString(), "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);


                                    Unterricht unterricht = new Unterricht(
                                        id,
                                        lehrer,
                                        fach,
                                        k.TrimEnd(','),
                                        r,
                                        "",
                                        zeitUndOrts[z].Tag,
                                        zeitUndOrts[z].Stunde,
                                        unterrichtsgruppeDiesesUnterrichts);

                                        this.Add(unterricht);
                                        try
                                        {
                                            string ugg = unterrichtsgruppeDiesesUnterrichts == null ? "" : unterrichtsgruppeDiesesUnterrichts.Name;
                                            Console.WriteLine(unterricht.Id.ToString().PadLeft(4) + " " + unterricht.LehrerKürzel.PadRight(4) + unterricht.KlasseKürzel.PadRight(20) + unterricht.FachKürzel.PadRight(10) + " Raum: " + r.PadRight(10) + " Tag: " + unterricht.Tag + " Stunde: " + unterricht.Stunde + " " + ugg.PadLeft(3));
                                        }
                                        catch (Exception ex)
                                        {

                                            throw;
                                        }
                                    
                                }
                            }
                        }
                    }
                    Console.WriteLine(("Unterrichte " + ".".PadRight(this.Count / 150, '.')).PadRight(48, '.') + (" " + this.Count).ToString().PadLeft(4), '.');

                    oleDbDataReader.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Fehler beim Unterricht mit der ID " + id + "\n" + ex.ToString());
                    throw new Exception("Fehler beim Unterricht mit der ID " + id + "\n" + ex.ToString());
                }
                finally
                {
                    oleDbConnection.Close();
                }
            }
        }

                
        public string GetHinweise(Klasses klasses, Verteilergruppen verteilergruppen, Lehrer lehrer, Anrechnungs anrechnungs)
        {
            string hinweise = "";

            hinweise += GetSchulleitung(lehrer, verteilergruppen, anrechnungs);
            hinweise += GetBereichsUndBildungsgangleitungen(lehrer, klasses, verteilergruppen, anrechnungs);
            hinweise += GetKlassenleitungen(lehrer,klasses);

            return hinweise;
        }

        private string GetSchulleitung(Lehrer lehrer, Verteilergruppen verteilergruppen, Anrechnungs anrechnungs)
        {
            string hinweise = "";

            if ((from a in anrechnungs where a.LehrerId == lehrer.IdUntis where a.Beschreibung.Contains("Schulleit") select a).Any())
            {
                var x = (from v in verteilergruppen where v.Name.Contains("Schulleit") select v).FirstOrDefault();

                if (x != null)
                {
                    var aa = (from a in anrechnungs where a.LehrerId == lehrer.IdUntis where a.Beschreibung.Contains("Schulleit") select a).FirstOrDefault();

                    hinweise = "<a target='_blank' rel='noopener noreferrer' title='" + aa.Beschreibung + "' href='" + x.Url + "'>" + aa.Beschreibung + "</a>";
                }                
            }
            return hinweise == "" ? "" : hinweise + "</br>";
        }

        private string GetBereichsUndBildungsgangleitungen(Lehrer lehrer, Klasses klasses, Verteilergruppen verteilergruppen, Anrechnungs anrechnungs)
        {            
            string hinweise = "";

            // Wenn der Lehrer BR oder BG-Leiterfunktion hat, 

            foreach (var brOderBglDiesesLehrers in (from a in anrechnungs.OrderBy(x => x.Kategorie)
                                                    where a.LehrerId == lehrer.IdUntis
                                                    where new List<string>()
                                                     {
                                                         "Bereichsleitung",
                                                         "Bildungsgangleitung"
                                                     }.Contains(a.Kategorie.ToString())
                                                    select a.Kategorie).Distinct().ToList())
            {
                // ... werden alle seine Bildungsgänge bzw. Bereiche verkettet ...

                var alleBgBzwBrInDenenDieserLehrerLeitet = (from a in anrechnungs
                                                            where a.LehrerId == lehrer.IdUntis
                                                            where a.Kategorie.ToString() == brOderBglDiesesLehrers.ToString()
                                                            select a).ToList();

                var zzz = (from v in verteilergruppen where v.Name == alleBgBzwBrInDenenDieserLehrerLeitet[0].Kategorie.ToString() select v).FirstOrDefault();
                
                if (zzz != null)
                {
                    hinweise += "<a target='_blank' rel='noopener noreferrer' title='" + zzz.Name + "' href='" + zzz.Url + "#" + lehrer.Kürzel + "'>" + zzz.Name + "</a>: ";
                }
                else
                {
                    hinweise += alleBgBzwBrInDenenDieserLehrerLeitet[0].Kategorie + ": ";
                }
                         
                foreach (var a in alleBgBzwBrInDenenDieserLehrerLeitet)
                {
                    // Wenn möglich, wird der URL ergänzt
                    
                    if ((from k in klasses where k.Beschreibung == a.Beschreibung select k).Any())
                    {
                        var x = (from k in klasses where k.Beschreibung == a.Beschreibung select k).FirstOrDefault();

                        if (x != null)
                        {
                            hinweise += "<a target='_blank' rel='noopener noreferrer' title='" + x.Beschreibung + "' href='" + x.Url + "'>" + x.Beschreibung + "</a>,";

                        }

                        // Wenn über die Klasse kein URL zu finden ist, dann evtl. über die Verteilergruppe

                        if (x == null)
                        {
                            var zz = (from v in verteilergruppen where v.Name == a.Beschreibung select v).FirstOrDefault();
                                                
                            if (zz != null)
                            {
                                hinweise += "<a target='_blank' rel='noopener noreferrer' title='" + zz.Name + "' href='" + zz.Url + "'>" + zz.Url + "</a>";
                            }
                            else
                            {
                                hinweise += a.Beschreibung + ",";
                            }
                        }                            
                    }
                    else
                    {
                            var zz = (from v in verteilergruppen where v.Name == a.Beschreibung select v).FirstOrDefault();

                            if (zz != null)
                            {
                                hinweise += "<a target='_blank' rel='noopener noreferrer' title='" + zz.Name + "' href='" + zz.Url + "'>" + zz.Name + "</a>";
                            }
                            else
                            {
                                hinweise += a.Beschreibung + ",";
                            }
                    }
                }
                hinweise = hinweise == "" ? "" : hinweise.TrimEnd(',') + "</br>";
            }
            return hinweise;
        }

        private string GetKlassenleitungen(Lehrer lehrer, Klasses klasses)
            {
            var klassen = (from k in klasses where k.Klassenleitungen != null where k.Klassenleitungen.Count > 0 where k.Klassenleitungen[0].Kürzel == lehrer.Kürzel select k).ToList();

            string klassenleitung = "";

            if (klassen.Count == 1)
            {
                klassenleitung += "Klassenleitung: ";
            }
            if (klassen.Count > 1)
            {
                klassenleitung += "Klassenleitungen: ";
            }

            foreach (var kla in klassen)
            {
                var jahrgang = kla.Jahrgang.EndsWith("01") ? " - Unterstufe" : kla.Jahrgang.EndsWith("02") ? " - Mittelstufe" : kla.Jahrgang.EndsWith("03") ? " - Oberstufe" : "";

                var x = " <a target='_blank' rel='noopener noreferrer' title='" + kla.Beschreibung + jahrgang + "' href='" + kla.Url + "'>" + kla.NameUntis + "</a>";

                klassenleitung += x + ",";
            }
            return klassenleitung.TrimEnd(',');
        }

        private string RenderVonBis(DateTime fBeginn, DateTime gEnde, string anmerkung)
        {
            if (anmerkung == null ||anmerkung == "")
            {
                return fBeginn.ToString("HH:mm") + "-" + gEnde.ToString("HH:mm");
            }
            return "";            
        }
        
        public List<Lehrer> GetLehrerMitUnterrichtUndMigrationskraftSortiert(Lehrers lehrers)
        {
            Lehrers lehrerMitUnterricht = new Lehrers();

            // Alle Lehrer, die auch unterrichten ...

            foreach (var unterricht in this)
            {
                if (!(from l in lehrerMitUnterricht where l.Kürzel == unterricht.LehrerKürzel select l).Any())
                {
                    if (unterricht.LehrerKürzel != "LAT")
                    {
                        lehrerMitUnterricht.Add((from l in lehrers where l.Kürzel == unterricht.LehrerKürzel select l).FirstOrDefault());
                    }
                }
            }

            lehrerMitUnterricht.Add((from l in lehrers where l.Funktion.StartsWith("Schulleiter") select l).FirstOrDefault());

            lehrerMitUnterricht.Add(new Lehrer("Frau", "Verena", "Baumeister", "BAM", "verena.baumeister@berufskolleg-borken.de", "1003"));
            lehrerMitUnterricht.Add(new Lehrer("Frau", "", "Dr. Wenz", "LWK NRW", "", "3301"));
            lehrerMitUnterricht.Add(new Lehrer("Frau", "", "Kessens", "LWK NRW", "", "3307"));

            Lehrers lehrerMitUnterrichtSortiert = new Lehrers();

            lehrerMitUnterrichtSortiert.AddRange(lehrerMitUnterricht.OrderBy(l => l.Nachname).ThenBy(l => l.Vorname));

            return lehrerMitUnterrichtSortiert;
        }

        public string GetFreieRäume(List<Lehrer> lehrerMitUnterrichtSortiert, Raums raums)
        {
            string freieRäume = "";

            foreach (var raum in (from r in raums select r).ToList())
            {
                if (!(from u in lehrerMitUnterrichtSortiert where u.Raum == raum.Raumnummer select u).Any())
                {
                    freieRäume += raum.Raumnummer + ",";
                }
            }
            return freieRäume.TrimEnd(',') + "\n";
        }

        public string GetMehrfachbelegteRäume(List<Lehrer> lehrerMitUnterrichtSortiert)
        {
        
            string mehrfachbelegteRäume = "";

            List<string> ra = new List<string>();

            foreach (var l in lehrerMitUnterrichtSortiert.OrderBy(x => x.Raum))
            {
                int anzahl = (from le in lehrerMitUnterrichtSortiert where l.Raum == le.Raum select le.Raum).Count();

                if (l.Raum != null && anzahl > 1 && !ra.Contains(l.Raum))
                {
                    mehrfachbelegteRäume += l.Raum + "(" + anzahl + "x),";
                    ra.Add(l.Raum);
                }
            }
            return mehrfachbelegteRäume.TrimEnd(',') + "\n";
        }

        public Raum GetRaum(Lehrer lehrer, Raums raums, Excelzeile excelzeile)
        {   
            if (excelzeile.MAnmerkungen != null && excelzeile.MAnmerkungen != "")
            {
                return new Raum();
            }

            // 1. Wahl ist der vorhandene Raum aus der Exceldatei

            if (excelzeile.HRaum != null && excelzeile.HRaum.Count > 0)
            {
                foreach (var raum in raums)
                {
                    if (excelzeile.HRaum[0].Raumnummer == raum.Raumnummer)
                    {
                        return raum;
                    }
                }

                return excelzeile.HRaum[0];
            }
            
            // 2. Wahl ist der Raum beim Lehrer in Untis
            
            foreach (var raum in raums)
            {
                if (lehrer.Raum == raum.Raumnummer)
                {
                    return raum;
                }
            }
            
            if (lehrer.Raum != "")
            {
                return new Raum(lehrer.Raum);
            }
            return new Raum();
        }
                
        internal Lehrers GetLehrer(List<string> beteiligteKlassen)
        {
            var beteiligteLehrers = new Lehrers();

            foreach (var unterricht in this)
            {
                foreach (var beteiligteKlasse in beteiligteKlassen)
                {
                    if (unterricht.KlasseKürzel.StartsWith(beteiligteKlasse))
                    {
                        beteiligteLehrers.Add((from l in Lehrers where unterricht.LehrerKürzel == l.Kürzel select l).FirstOrDefault());
                    }
                }
            }
            return beteiligteLehrers;
        }

        internal List<Unterricht> SortierenUndKumulieren()
        {
            try
            {
                // Die Unterrichte werden chronologisch sortiert

                List<Unterricht> sortierteUnterrichts = (from u in this
                                                         orderby u.KlasseKürzel, u.FachKürzel, u.Raum, u.Tag, u.Stunde
                                                         select u).ToList();

                for (int i = 0; i < sortierteUnterrichts.Count; i++)
                {
                    // Wenn es einen nachfolgenden Unterricht gibt ...

                    if (i < sortierteUnterrichts.Count - 1)
                    {
                        // ... und dieser in allen Eigenschaften identisch ist ...

                        if (sortierteUnterrichts[i].KlasseKürzel == sortierteUnterrichts[i + 1].KlasseKürzel && sortierteUnterrichts[i].FachKürzel == sortierteUnterrichts[i + 1].FachKürzel && sortierteUnterrichts[i].Raum == sortierteUnterrichts[i + 1].Raum)
                        {
                            // ... und der nachfolgende Unterricht unmittelbar (nach der Pause) anschließt ... 

                            if (sortierteUnterrichts[i].Bis == sortierteUnterrichts[i + 1].Von || sortierteUnterrichts[i].Bis.AddMinutes(15) == sortierteUnterrichts[i + 1].Von || sortierteUnterrichts[i].Bis.AddMinutes(20) == sortierteUnterrichts[i + 1].Von)
                            {
                                // ... wird der Beginn des Nachfolgers nach vorne geschoben ...

                                sortierteUnterrichts[i + 1].Von = sortierteUnterrichts[i].Von;

                                // ... und der Vorgänger wird gelöscht.

                                sortierteUnterrichts.RemoveAt(i);

                                // Der Nachfolger bekommt den Index des Vorgängers.
                                i--;
                            }
                        }
                    }
                }
                return (
                    from s in sortierteUnterrichts
                    orderby s.Tag, s.Stunde, s.KlasseKürzel
                    select s).ToList();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                throw new Exception(ex.ToString());
            }
        }
         
        public Unterrichts Kumulieren()
        {
            try
            {
                for (int i = 0; i < this.Count; i++)
                {
                    // Wenn es einen nachfolgenden Unterricht gibt ...

                    if (i < this.Count - 1)
                    {
                        // ... und dieser in allen Eigenschaften identisch ist ...

                        if (this[i].KlasseKürzel == this[i + 1].KlasseKürzel && this[i].FachKürzel == this[i + 1].FachKürzel && this[i].Raum == this[i + 1].Raum)
                        {
                            // ... und der nachfolgende Unterricht unmittelbar (nach der Pause) anschließt ... 

                            if (this[i].Bis == this[i + 1].Von || this[i].Bis.AddMinutes(15) == this[i + 1].Von || this[i].Bis.AddMinutes(20) == this[i + 1].Von)
                            {
                                // ... wird der Beginn des Nachfolgers nach vorne geschoben ...

                                this[i + 1].Von = this[i].Von;

                                // ... und der Vorgänger wird gelöscht.

                                this.RemoveAt(i);

                                // Der Nachfolger bekommt den Index des Vorgängers.
                                i--;
                            }
                        }
                    }
                }
                return this;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                throw new Exception(ex.ToString());
            }
        }
    }
}