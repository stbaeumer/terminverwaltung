﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace Terminverwaltung
{
    public class Tabellenzeile
    {
        public int Zeile { get; private set; }
        public DateTime ADatum { get; private set; }
        public string BTag { get; private set; }
        public string CVonBis { get; private set; }
        public string DBeschreibung { get; private set; }
        public string EJahrgang { get; private set; }
        public DateTime FBeginn { get; private set; }
        public DateTime GEnde { get; private set; }
        public Raums HRaum { get; private set; }
        public List<object> IVerantwortlich { get; private set; }
        public List<string> JKategorie { get; private set; }
        public string MAnmerkungen { get; private set; }
        public string LDokumente { get; private set; }
        public string KHinweise { get; private set; }
        public string LSortierung { get; private set; }

        public Tabellenzeile()
        {
        }

        public Tabellenzeile(Verteilergruppen verteilergruppen, Lehrers lehrers, Klasses klasses, Raums raums, int zeile, string aDatum, string bTag, string cVonBis, string dBeschreibung, string eJahrgang, string fBeginn, string gEnde, Raums hRaum, string iVerantwortlich, string jKategorie, string kHinweise, string lDokumente, string mAnmerkungen)
        {
            Zeile = zeile;
            ADatum = DateTime.ParseExact(aDatum, "dd.MM.yyyy", CultureInfo.InvariantCulture);
            BTag = ADatum.ToString("ddd, dd.MM.yyyy", System.Globalization.CultureInfo.InvariantCulture);
            CVonBis = cVonBis;
            DBeschreibung = dBeschreibung;
            EJahrgang = eJahrgang;
            FBeginn = ADatum.AddHours(DateTime.ParseExact(fBeginn, "HH:mm", CultureInfo.InvariantCulture).Hour).AddMinutes(DateTime.ParseExact(fBeginn, "HH:mm", CultureInfo.InvariantCulture).Minute);            
            GEnde = ADatum.AddHours(DateTime.ParseExact(gEnde, "HH:mm", CultureInfo.InvariantCulture).Hour).AddMinutes(DateTime.ParseExact(gEnde, "HH:mm", CultureInfo.InvariantCulture).Minute);
            //HRaum = FilterRaum(hRaum, raums);
            if (dBeschreibung.Contains("Zulassungskonferenz"))
            {
                string a = "";
            }
            IVerantwortlich = FilterVerantwortliche(iVerantwortlich, verteilergruppen, lehrers, klasses);
            JKategorie = new List<string>();
            foreach (var kategorie in jKategorie.Split(' '))
            {
                JKategorie.Add(kategorie.Trim());
            }
            
            KHinweise = kHinweise;
            LDokumente = lDokumente;
            MAnmerkungen = mAnmerkungen;
        }

        private Raums FilterRaum(string hRaum, Raums raums)
        {
            Raums rs = new Raums();

            foreach (var r in raums)
            {
                if (hRaum.Contains(r.Raumname))
                {
                    rs.Add(r);
                }
            }
            return rs;
        }

        public List<object> FilterVerantwortliche(string iVerantwortlich, Verteilergruppen verteilergruppen, Lehrers lehrers, Klasses klasses)
        {
            List<object> verantwortliche = new List<object>();

            foreach (var l in lehrers)
            {
                if (iVerantwortlich.Contains(l.Nachname) 
                    || iVerantwortlich.Contains(l.Kürzel) 
                    || iVerantwortlich.Contains(l.Mail))
                {
                    verantwortliche.Add(l);
                }
            }
            foreach (var k in klasses)
            {
                if (iVerantwortlich.Contains(k.NameUntis))
                {
                    verantwortliche.Add(k);
                }
            }
            foreach (var v in verteilergruppen)
            {
                if (iVerantwortlich.Contains(v.Name) || iVerantwortlich.Contains(v.Mail))
                {
                    verantwortliche.Add(v);
                }
            }
            return verantwortliche;
        }

        public Tabellenzeile(int zeile, DateTime aDatum, string bTag, string cVonBis, string dBeschreibung, string eJahrgang, DateTime fBeginn, DateTime gEnde, Raums hRaum, List<object> iVerantwortlich, List<string> jKategorie, string kHinweise, string lDokumente, string mAnmerkungen)
        {
            Zeile = zeile;
            ADatum = aDatum;
            BTag = bTag;
            CVonBis = cVonBis;
            DBeschreibung = dBeschreibung;
            EJahrgang = eJahrgang;
            FBeginn = fBeginn;
            GEnde = gEnde;
            HRaum = hRaum;
            IVerantwortlich = iVerantwortlich;
            JKategorie = jKategorie;
            KHinweise = kHinweise;
            LDokumente = lDokumente;
            MAnmerkungen = mAnmerkungen;
        }
    }
}