﻿using System.Collections.Generic;

namespace Terminverwaltung
{
    internal class Jon
    {
        

        public Jon()
        {
        }

        public string id { get; internal set; }
        public string name { get; internal set; }
        public string description { get; internal set; }
        public string author { get; internal set; }
        public string last_modified { get; internal set; }
        public List<List<string>> data { get; internal set; }
        public Optionen options { get; internal set; }
        public Visibility visibility { get; internal set; }
    }
}