﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Terminverwaltung
{
    public class Pläne : List<Plan>
    {
        public Feriens Feriens { get; private set; }

        public Pläne(Feriens feriens, Lehrers lehrers, Klasses klassen, Unterrichts unterrichts)
        {
            Feriens = feriens;

            DirectoryInfo d = new DirectoryInfo(@"\\fs01\Schulverwaltung\Terminverwaltung");

            Console.Write(("Terminpläne").PadRight(70, '.'));

            foreach (var datei in d.GetFiles("*.csv"))
            {                
                this.Add(new Plan(datei.ToString(), klassen, unterrichts, lehrers, feriens));
            }

            Console.WriteLine((" " + this.Count.ToString()).PadLeft(30, '.'));
        }
    }
}