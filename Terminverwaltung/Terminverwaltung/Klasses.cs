﻿using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Globalization;
using System.IO;
using System.Linq;

namespace Terminverwaltung
{
    public class Klasses : List<Klasse>
    {
        public Lehrers Lehrers { get; set; }

        public Klasses(Lehrers lehrers, Raums raums, Periodes periodes)
        {
            Lehrers = lehrers;

            using (OleDbConnection oleDbConnection = new OleDbConnection(Global.ConU))
            {
                try
                {
                    string queryString = @"SELECT 
Class.CLASS_ID, 
Class.Name, 
Class.TeacherIds, 
Class.Longname, 
Teacher.Name,
Class.ClassLevel, 
Class.PERIODS_TABLE_ID,
Department.Name,
Class.TimeRequest,
Class.ROOM_ID,
Class.Text
FROM (Class LEFT JOIN Department ON Class.DEPARTMENT_ID = Department.DEPARTMENT_ID) LEFT JOIN Teacher ON Class.TEACHER_ID = Teacher.TEACHER_ID
WHERE (((Class.SCHOOL_ID)=177659) AND ((Class.TERM_ID)=" + periodes.Count + ") AND ((Class.Deleted)=False) AND ((Class.TERM_ID)=" + periodes.Count + ") AND ((Class.SCHOOLYEAR_ID)=" + Global.AktSjUnt + ") AND ((Department.SCHOOL_ID)=177659) AND ((Department.SCHOOLYEAR_ID)=" + Global.AktSjUnt + ") AND ((Teacher.SCHOOL_ID)=177659) AND ((Teacher.SCHOOLYEAR_ID)=" + Global.AktSjUnt + ") AND ((Teacher.TERM_ID)=" + periodes.Count + "))ORDER BY Class.Name ASC; ";

                    OleDbCommand oleDbCommand = new OleDbCommand(queryString, oleDbConnection);
                    oleDbConnection.Open();
                    OleDbDataReader oleDbDataReader = oleDbCommand.ExecuteReader();

                    while (oleDbDataReader.Read())
                    {
                        List<Lehrer> klassenleitungen = new List<Lehrer>();

                        foreach (var item in (Global.SafeGetString(oleDbDataReader, 2)).Split(','))
                        {
                            klassenleitungen.Add((from l in lehrers
                                                  where l.IdUntis.ToString() == item
                                                  select l).FirstOrDefault());
                        }

                        var klasseName = Global.SafeGetString(oleDbDataReader, 1);
                        
                        Klasse klasse = new Klasse()
                        {
                            IdUntis = oleDbDataReader.GetInt32(0),
                            NameUntis = klasseName,
                            Klassenleitungen = klassenleitungen,
                            Jahrgang = Global.SafeGetString(oleDbDataReader, 5),
                            Bereichsleitung = Global.SafeGetString(oleDbDataReader, 7),
                            Beschreibung = Global.SafeGetString(oleDbDataReader, 3),
                            Berufsschultage = GetBerufsschultage(Global.SafeGetString(oleDbDataReader, 5), Global.SafeGetString(oleDbDataReader, 8)),
                            Raum = (from r in raums where r.IdUntis == oleDbDataReader.GetInt32(9) select r).FirstOrDefault(),
                            Url = "https://www.berufskolleg-borken.de/bildungsgange/" + Global.SafeGetString(oleDbDataReader, 10)
                        };

                        this.Add(klasse);
                    };

                    Console.WriteLine(("Klassen " + ".".PadRight(this.Count / 150, '.')).PadRight(48, '.') + (" " + this.Count).ToString().PadLeft(4), '.');

                    oleDbDataReader.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    throw new Exception(ex.ToString());
                }
                finally
                {
                    oleDbConnection.Close();
                }
            }
        }

        internal IEnumerable<Excelzeile> APA(Lehrers lehrers, Excelzeilen excelzeilenQuelle)
        {
            throw new NotImplementedException();
        }

        internal IEnumerable<Excelzeile> Blockpläne(Excelzeilen excelzeilen, Unterrichts unterrichts, Unterrichtsgruppes unterrichtsgruppes)
        {
            throw new NotImplementedException();
        }

        public Klasses()
        {
        }

        internal void ErsterSchultag(Unterrichts unterrichts, DateTime ersterSchultag, List<string> Block)
        {
            foreach (var klasse in this)
            {                
                if (klasse.Jahrgang.StartsWith("BS"))
                {
                    var w = (from u in unterrichts where u.KlasseKürzel == klasse.NameUntis where u.Unterrichtsgruppe != null where Block.Contains(u.Unterrichtsgruppe.Name) select u.Unterrichtsgruppe).FirstOrDefault();
                                        
                    // Blockklassen

                    if (w != null)
                    {
                        klasse.ErsterSchultag = w.Von;
                    }
                    else
                    {
                        // Alle Landwirte kommen am ersten Schultag

                        if (klasse.NameUntis.StartsWith("AL"))
                        {
                            klasse.ErsterSchultag = ersterSchultag;
                        }
                        else
                        {
                            if (!klasse.Jahrgang.EndsWith("1"))
                            {
                                // Fortgeführte Berufsschulklassen kommen nicht am ersten Schultag.
                                // Fortgeführte Berufsschulklassen kommen an ihrem ersten Berufsschultag

                                for (int i = 14; i > 0; i--)
                                {
                                    DateTime x = ersterSchultag.AddDays(i);

                                    // Für jeden Berufsschultag dieser Klasse wird geprüft, ob er auf x fällt.

                                    foreach (var berufsschultag in klasse.Berufsschultage)
                                    {
                                        // Wenn der Turnus stimmt, wird das Datum zurückgegeben

                                        if (berufsschultag.Wochentag.ToLower().Substring(0,6) == Wochentag(x).ToLower().Substring(0, 6))
                                        {
                                            if (berufsschultag.Turnus == "wöchentlich" || (IstGeradeKw(ersterSchultag) && berufsschultag.Turnus == "gerade KW") || (!IstGeradeKw(ersterSchultag) && berufsschultag.Turnus == "ungerade KW"))
                                            {
                                                klasse.ErsterSchultag = x;
                                            }
                                        }                                        
                                    }
                                }
                            }
                            else
                            {
                                // Alle Unterstufen und alle Vollzeitklassen kommen am ersten Schultag

                                klasse.ErsterSchultag = ersterSchultag;                                
                            }
                        }
                    }
                }
                else
                {
                    // Vollzeitklassen

                    if (klasse.Jahrgang.EndsWith("1"))
                    {
                        klasse.ErsterSchultag = ersterSchultag;
                    }
                    else
                    {
                        klasse.ErsterSchultag = ersterSchultag.AddDays(1);
                    }
                }             
            }
        }

        public Excelzeilen Notenlisten(Schuelers schuelers, Lehrers lehrers, ConsoleKey key)
        {   
            List<string> alleDateien = new List<string>();

            try
            {
                Global.Dateien = new List<string>();

                schuelers = schuelers ?? new Schuelers();

                foreach (var klasse in (from k in this where ((from s in schuelers
                                                               where s.Klasse.NameUntis == k.NameUntis
                                                               where (from g in Global.AbschlussKlassen where s.Klasse.NameUntis.StartsWith(g) select g).Any()
                                                               select s).Any()) select k).ToList())
                {
                    var teilnehmer = new List<Lehrer>
                    {
                        (from l in lehrers where l.Kürzel == Global.KürzelSchulleiter select l).FirstOrDefault(),
                        (from l in lehrers where l.Kürzel == klasse.Bereichsleitung select l).FirstOrDefault()
                    };

                    teilnehmer.AddRange(klasse.Klassenleitungen);
                    var beginn = (from g in Global.ApaUhrzeiten where g.Key == klasse.NameUntis select g.Value).FirstOrDefault();

                    if (!(key == ConsoleKey.Q))
                    {
                        Global.WaitForFile(@"C:\Users\bm\Berufskolleg Borken\Terminplanung - Documents\Pruefungen\APA.xlsx");
                        if (File.Exists(Global.Ziel))
                            Global.WaitForFile(Global.Ziel);
                        System.IO.File.Copy(@"C:\Users\bm\Berufskolleg Borken\Terminplanung - Documents\Pruefungen\APA.xlsx", Global.Ziel, true);
                        Application application = new Application();
                        Workbook workbook = application.Workbooks.Open(Global.Ziel);
                        klasse.Notenliste(application, workbook, (from s in schuelers
                                                                  where s.Klasse.NameUntis == klasse.NameUntis
                                                                  select s).ToList(), lehrers, teilnehmer, beginn);

                        workbook.Save();
                        workbook.Close();
                        application.Quit();
                    }
                }

                // Alle Dateien werden mir zugesandt

                Global.MailSenden();

                
            }
            catch (Exception ex)
            {                
                Console.WriteLine(ex);
            }            
            return null;
        }

        internal Klasse GetKlasseFromString(string dBeschreibung)
        {
            foreach (var klasse in this)
            {
                if (dBeschreibung == klasse.NameUntis)
                {
                    return klasse;
                }
            }

            foreach (var klasse in this)
            {
                if (dBeschreibung == klasse.NameUntis.Substring(0, klasse.NameUntis.Length - 1))
                {
                    return klasse;
                }
            }
            foreach (var klasse in this)
            {
                if (dBeschreibung == klasse.NameUntis.Substring(0, klasse.NameUntis.Length - 2))
                {
                    return klasse;
                }
            }
            return new Klasse();
        }

        internal Klasse FilterKlasse(List<object> verantwortlich)
        {
            foreach (var eV in verantwortlich)
            {
                if (eV.GetType() == typeof(Klasse))
                {
                    return (from k in this where ((Klasse)eV).NameUntis == k.NameUntis select k).FirstOrDefault();
                }
            }
            return null;
        }

        internal IEnumerable<Excelzeile> GetVoreinschulung(Unterrichts unterrichts, Lehrers lehrers)
        {
            // Kategorie

            //excelzeilen.Add(new Excelzelle(zeile, spalte + 8, "Voreinschulung"));
            return new List<Excelzeile>();
        }

        internal List<Excelzeile> Zeugniskonferenzen(Unterrichts unterrichts, Lehrers lehrers, Excelzeilen excelzeilenQuelle, Raums raums)
        {
            Zeugniskonferenzsitzungen zeugniskonferenzsitzungen = new Zeugniskonferenzsitzungen
            {
                new Zeugniskonferenzsitzung("Zeugniskonferenzen HZBC", "Gesundheit und Soziales", (from k in this where k.Bereichsleitung == "HE" select k).OrderBy(x => x.Jahrgang).ToList(), (from r in raums where r.Raumnummer == "1102" select r).FirstOrDefault()),
                new Zeugniskonferenzsitzung("Zeugniskonferenzen HZBC", "Technik",(from k in this where k.Bereichsleitung == "NX" select k).OrderBy(x => x.Jahrgang).ToList(), (from r in raums where r.Raumnummer == "1104" select r).FirstOrDefault()),
                new Zeugniskonferenzsitzung("Zeugniskonferenzen HZBC", "Wirtschaft und Verwaltung", (from k in this where k.Bereichsleitung == "SC" select k).OrderBy(x => x.NameUntis).ToList(), (from r in raums where r.Raumnummer == "1101" select r).FirstOrDefault()),
                new Zeugniskonferenzsitzung("Zeugniskonferenzen HZD", "Berufliches Gymnasium", (from k in this where k.Bereichsleitung == "GB" select k).OrderBy(x => x.NameUntis).ToList(), (from r in raums where r.Raumnummer == "1101" select r).FirstOrDefault()),
                new Zeugniskonferenzsitzung("Zeugniskonferenzen JZBC", "Gesundheit und Soziales", (from k in this where k.Bereichsleitung == "HE" && !k.NameUntis.Contains("O") && !k.NameUntis.Contains("12") select k).OrderBy(x => x.Jahrgang).ToList(), (from r in raums where r.Raumnummer == "1102" select r).FirstOrDefault()),
                new Zeugniskonferenzsitzung("Zeugniskonferenzen JZBC", "Technik",(from k in this where k.Bereichsleitung == "NX" && !k.NameUntis.Contains("O") && !k.NameUntis.Contains("12") select k).OrderBy(x => x.Jahrgang).ToList(), (from r in raums where r.Raumnummer == "1104" select r).FirstOrDefault()),
                new Zeugniskonferenzsitzung("Zeugniskonferenz JZBC", "Wirtschaft und Verwaltung", (from k in this where k.Bereichsleitung == "SC" && !k.NameUntis.Contains("O") select k).OrderBy(x => x.NameUntis).ToList(), (from r in raums where r.Raumnummer == "1101" select r).FirstOrDefault()),
                new Zeugniskonferenzsitzung("Zeugniskonferenzen JZD", "Berufliches Gymnasium", (from k in this where k.Bereichsleitung == "GB" && !k.NameUntis.Contains("13") select k).OrderBy(x => x.NameUntis).ToList(), (from r in raums where r.Raumnummer == "1101" select r).FirstOrDefault()),

                // APA

                new Zeugniskonferenzsitzung(
                    "Zulassungskonferenzen FHR", 
                    "FHR", 
                (from k in this
                    where k.NameUntis.Contains("HBTO") || 
                    k.NameUntis.Contains("HHO") ||
                    k.NameUntis.Contains("HBFGO") ||
                    k.NameUntis.StartsWith("12") ||
                    k.NameUntis.Contains("BSO")
                 select k).OrderBy(x => x.Bereichsleitung).OrderBy(x => x.NameUntis).ToList(), 
                (from r in raums where r.Raumnummer == "1101" select r).FirstOrDefault())
            };
            List<Excelzeile> excelzeilen = new List<Excelzeile>();

            try
            {
                foreach (var zk in zeugniskonferenzsitzungen)
                {
                    DateTime datum = (from t in excelzeilenQuelle
                                     where t.JKategorie.Count > 0
                                     where t.JKategorie[0] == zk.Zk
                                     select t.ADatum).FirstOrDefault();

                    if (datum == null)
                    {
                        throw new Exception("Es kann für " + zk.Zk + " kein Datum ausgelesen werden.");
                    }

                    DateTime z = (from t in excelzeilenQuelle
                                      where t.JKategorie.Count > 0
                                      where t.JKategorie[0] == zk.Zk
                                      select t.FBeginn).FirstOrDefault();

                    var zeit = new DateTime(datum.Year, datum.Month, datum.Day, z.Hour, z.Minute, z.Second);

                    foreach (var k in zk.Klassen)
                    {
                        var aDatum = datum;
                        var bTag = "";
                        var cVonBis = "";
                        var dBeschreibung = GetKlasseBeschreibung(zk.Zk, k);
                        var eJahrgang = "";
                        var fBeginn = zeit;
                        var gEnde = fBeginn.AddMinutes(10);
                        var hRaum = new Raums
                            {
                                zk.Raum
                            };
                        var iVerantwortlich = new List<object>();
                        
                        if (zk.Zk.Contains("APA"))
                        {
                            iVerantwortlich.Add("Schulleitung");
                            iVerantwortlich.Add((from l in lehrers where k.Bereichsleitung == l.Kürzel select l).FirstOrDefault());                            
                        }

                        iVerantwortlich.AddRange(k.Klassenleitungen);
                        List<string> jKategorie = new List<string> { zk.Zk };

                        var kHinweise = "";
                        var lGeschützt = "";
                        var mAnmerkungen = "";

                        excelzeilen.Add(new Excelzeile(aDatum, bTag, cVonBis, dBeschreibung, eJahrgang, fBeginn, gEnde, hRaum, iVerantwortlich, jKategorie, kHinweise, lGeschützt, mAnmerkungen, "", ""));
                        zeit = gEnde;
                    }
                }                
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Console.ReadKey();
            }

            return excelzeilen;
        }
        
        private string GetRaum(Klasse k)
        {
            string raum = k.Bereichsleitung == "HE" ? "1104" : k.Bereichsleitung == "NX" ? "1105" : "1107";

            try
            {
                string tooltiptext = "Gebäude " + raum.Substring(0, 1) + " | "
                 + (raum.Substring(1, 1) == "0" ? "Erdgeschoss" : raum.Substring(1, 1) + ". Etage ");

                return "<dfn title='" + tooltiptext + "'>" + raum + "</dfn>";
            }
            catch (Exception)
            {
                Console.WriteLine("Die Konferenz " + k.NameUntis + " hat keinen Raum. Bitte eintragen. ENTER");
                return "";
            }
        }

        public string GetKlasseBeschreibung(string zk, Klasse k)
        {
            string inhalt = "<b>" + zk.Split(' ')[0] + " <a href='" + k.Url + "'>" + k.NameUntis + "</a> - <a href='" + k.Url + "'>" + k.Beschreibung + "</a></b></br>Klassenleitung:";

            for (int p = 0; p < k.Klassenleitungen.Count; p++)
            {
                string anrede = (k.Klassenleitungen[p].Anrede == "Frau" ? "Frau" : "Herrn") + " " + k.Klassenleitungen[p].Titel + (k.Klassenleitungen[p].Titel == "" ? "" : " ") + k.Klassenleitungen[p].Nachname;
                inhalt += " <a title='Nachricht für " + anrede + "' href='mailto: " + k.Klassenleitungen[p].Mail + " ?subject=Nachricht für " + anrede + "'>" + k.Klassenleitungen[p].Kürzel + "</a> ";
            }
            return inhalt.TrimEnd(',');
        }

        public List<Excelzeile> Berufsschultage(Unterrichts unterrichts, Unterrichtsgruppes unterrichtsgruppes, DateTime ersterSchultag)
        {
            var excelzeilenSenke = new Excelzeilen();

            try
            {                
                List <string> alleBerufe = new List<string>();
                alleBerufe.AddRange((from k in this where k.Berufsschultage.Count > 0 || k.NameUntis.StartsWith("AG") select k.Beschreibung).Distinct().ToList());

                Klasses klasses = new Klasses();
                klasses.AddRange((from k in this where k.Jahrgang.StartsWith("BS") || k.Jahrgang.StartsWith("AV") select k).ToList());

                klasses.ÜberprüfeBerufsschultage(unterrichts, unterrichtsgruppes);

                foreach (var beruf in alleBerufe)
                {
                    foreach (var klasse in (from k in klasses where k.Beschreibung == beruf select k).OrderBy(s => s.Jahrgang).ToList())
                    {                        
                        DateTime aDatum = klasse.ErsterSchultag;
                        string cVonBis = "";
                        string dBeschreibung = "<b><a href='" + klasse.Url + "'>" + klasse.NameUntis + " - " + klasse.Beschreibung + " - " + GetStufe(klasse) + "</a></b>";
                        string eJahrgang = (klasse.Jahrgang.EndsWith("1") ? "Unterstufe" : klasse.Jahrgang.EndsWith("2") ? "Mittelstufe" : klasse.Jahrgang.EndsWith("3") ? "Oberstufe" : "Abschlussklasse");
                        DateTime fBeginn = klasse.NameUntis.StartsWith("ALM") || klasse.NameUntis.StartsWith("ALO") ? new DateTime(aDatum.Year, aDatum.Month, aDatum.Day, 11, 0, 0) : new DateTime(aDatum.Year, aDatum.Month, aDatum.Day, 7, 40, 0);
                        DateTime gEnde = aDatum;
                        Raums hRaum = new Raums
            {
                new Raum("BKB Forum")
            };
                        string kHinweis = GetInhaltBerufsschultage(klasse);

                        List<string> jKategorie = new List<string>() { "ErsterSchultag", "Berufsschultage" };
                        
                        excelzeilenSenke.Add(new Excelzeile(aDatum, "", cVonBis, dBeschreibung, eJahrgang, fBeginn, gEnde, hRaum, new List<object>(), jKategorie, kHinweis, "", "","",""));
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Console.ReadKey();
            }

            return excelzeilenSenke;
        }

        private void ÜberprüfeBerufsschultage(Unterrichts unterrichts, Unterrichtsgruppes unterrichtsgruppes)
        {
            foreach (var klasse in this)
            {
                try
                {
                    if (klasse.Berufsschultage.Count > 0)
                    {
                        if (klasse.NameUntis.StartsWith("MMU1"))
                        {
                            string a = ";";
                        }
                        foreach (var berufsschutag in klasse.Berufsschultage)
                        {
                            if (berufsschutag.Turnus != "wöchentlich")
                            {
                                // Prüfe alle Unterrichte des Klasse, die auf den Rolltag in das 2. HJ fallen.

                                var z = 0;
                                if (berufsschutag.Wochentag.ToLower().StartsWith("mo"))
                                {
                                    z = 1;
                                }
                                if (berufsschutag.Wochentag.ToLower().StartsWith("di"))
                                {
                                    z = 2;
                                }
                                if (berufsschutag.Wochentag.ToLower().StartsWith("mi"))
                                {
                                    z = 3;
                                }
                                if (berufsschutag.Wochentag.ToLower().StartsWith("do"))
                                {
                                    z = 4;
                                }
                                if (berufsschutag.Wochentag.ToLower().StartsWith("fr"))
                                {
                                    z = 5;
                                }

                                var zu = (from u in unterrichts
                                          where u.KlasseKürzel == klasse.NameUntis
                                          where u.Unterrichtsgruppe != null
                                          where u.Unterrichtsgruppe.Von.Month <= 2
                                          where u.Tag == z
                                          select (u.Bis - u.Von).TotalMinutes).Sum();
                                

                                if (berufsschutag.Turnus == "gerade KW" && zu > 180)
                                {
                                    berufsschutag.Turnus = "nur im 2. Halbjahr";

                                    Console.WriteLine("Der Turnus der Klasse " + klasse.NameUntis + " wurde geändert von " + berufsschutag.Wochentag + " '" + berufsschutag.Turnus + "' nach '2.HJ' geändert");
                                    //Console.ReadKey();
                                }

                                zu = (from u in unterrichts
                                          where u.KlasseKürzel == klasse.NameUntis
                                          where u.Unterrichtsgruppe != null
                                          where u.Unterrichtsgruppe.Bis.Month <= 2
                                          where u.Tag == z
                                          select (u.Bis - u.Von).TotalMinutes).Sum();


                                if (berufsschutag.Turnus.Contains("ungerade") && zu > 180)
                                {
                                    Console.WriteLine("Der Turnus der Klasse " + klasse.NameUntis + " wurde geändert von  " + berufsschutag.Wochentag + " '" + berufsschutag.Turnus+ "' nach '1.HJ' geändert");

                                    berufsschutag.Turnus = "nur im 1. Halbjahr";
                                    //Console.ReadKey();
                                }

                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }                
            }
        }

        public Excelzeile ErsterSchultagNeueVollzeitklassen(DateTime ersterSchultag)
        {            
            DateTime aDatum = ersterSchultag;
            string cVonBis = "";
            string dBeschreibung = "<b><font color='green'>Erster Schultag für alle neueinsteigenden Vollzeitklassen</font>.</b></br>Bitte Aushänge vor Ort beachten.";
            string eJahrgang = "";
            DateTime fBeginn = new DateTime(aDatum.Year,aDatum.Month,aDatum.Day,11,0,0);
            DateTime gEnde = aDatum;
            Raums hRaum = new Raums
            {
                new Raum("BKB Forum")
            };
            List<string> jKategorie = new List<string>() { "ErsterSchultag" };
                  
            return new Excelzeile(aDatum, "", cVonBis, dBeschreibung, eJahrgang, fBeginn, gEnde, hRaum, new List<object> (), jKategorie, "", "", "","","");
        }

        public Excelzeile ErsterSchultagFortgeführteVollzeitklassen(DateTime ersterSchultag)
        {   
            DateTime aDatum = ersterSchultag.AddDays(1);
            string cVonBis = "";
            string dBeschreibung = "<b><font color='green'> Erster Schultag für alle fortgeführten Vollzeitklassen</font></b> </br>laut Stundenplan im <a href='https://nessa.webuntis.com/WebUntis/?school=bk-borken#/basic/main'>DigiKlas</a>.";
            string eJahrgang = "";
            DateTime fBeginn = new DateTime(aDatum.Year, aDatum.Month, aDatum.Day, 7, 40, 0);
            DateTime gEnde = aDatum;
            Raums hRaum = new Raums
            {
                new Raum("BKB Forum")
            };
            List<string> jKategorie = new List<string>() { "ErsterSchultag" };
            
            return new Excelzeile(aDatum, "", cVonBis, dBeschreibung, eJahrgang, fBeginn, gEnde, hRaum, new List<object>(), jKategorie, "", "", "","","");
        }
        
        private string Wochentag(DateTime x)
        {   
                return string.Format("{0:dddd}", x);   
        }

        private bool IstGeradeKw(DateTime ersterSchultag)
        {
            CultureInfo CUI = CultureInfo.CurrentCulture;
            var kw = CUI.Calendar.GetWeekOfYear(ersterSchultag, CUI.DateTimeFormat.CalendarWeekRule, CUI.DateTimeFormat.FirstDayOfWeek);

            return kw == 2 ? true : false;
        }
        
        private string GetInhaltBerufsschultage(Klasse klasse)
        {
            var bt = "";

            if (klasse.NameUntis.StartsWith("AGG"))
            {
                bt = "Berufsschultage siehe <a href='https://www.berufskolleg-borken.de/termine/blockzeiten/'>Blockzeiten.";
            }

            foreach (var item in klasse.Berufsschultage.OrderByDescending(z => z.Turnus))
            {
                bt += (item.Turnus.Contains("wöchentlich") ? "<font color=\"blue\"> " + item.Wochentag + "</font> +" : item.Turnus.Contains("ngerade") ? "<font color=\"blue\"> " + item.Wochentag + " (" + item.Turnus + ")</font> +" : "<font color=\"blue\"> " + item.Wochentag + " (" + item.Turnus + ")</font>+");
            }
            
            return bt.TrimEnd('+');
        }

        private string GetStufe(Klasse klasse)
        {
            if (klasse.Jahrgang.EndsWith("1"))
            {
                return "Unterstufe";
            }
            if (klasse.Jahrgang.EndsWith("2"))
            {
                return "Mittelstufe";
            }
            if (klasse.Jahrgang.EndsWith("3"))
            {
                return "Oberstufe";
            }
            if (klasse.Jahrgang.EndsWith("4"))
            {
                return "Abschlussklasse";
            }
            return "";
        }

        private Berufsschultages GetBerufsschultage(string classlevel, string timerequest)
        {
            var berufsschultages = new Berufsschultages();

            // Wenn es sich um eine Berufsschulklasse handelt

            if (classlevel.Split('-')[0] == "BS")
            {
                foreach (var item in timerequest.Split(','))
                {
                    // Wenn in der ersten Stunde 

                    if (item.Length > 0 && item.Split('~')[1] == "1")
                    {
                        // ... ein Zeitwunsch von +1, +2 oder +3 steht.

                        if ((new List<string>() {"4","5","6" }).Contains(item.Split('~')[0]))                        
                        {
                            var wochentag = item.Split('~')[3];
                            var turnus = item.Split('~')[0] == "4" ? "ungerade KW" : item.Split('~')[0] == "5" ? "gerade KW" : "wöchentlich";
                            
                            // Wenn der Turnus gearde oder ungerade ist, müssen die Unterrichte auf deren Unterrichtsgruppe geprüft werden.
                            // Denkbar ist auch ein halbjährlicher Turnus



                            berufsschultages.Add(new Berufsschultag(wochentag, turnus));
                        }
                    }
                }
            }
            return berufsschultages;
        }

        internal Lehrers GetBereichleiter(List<string> beteiligteKlassen)
        {
            var beteiligteLehrers = new Lehrers();

            foreach (var klasse in this)
            {
                foreach (var beteiligteKlasse in beteiligteKlassen)
                {
                    if (klasse.NameUntis.StartsWith(beteiligteKlasse))
                    {
                        beteiligteLehrers.Add((from l in Lehrers where klasse.Bereichsleitung == l.Kürzel select l).FirstOrDefault());
                    }
                }
            }
            return beteiligteLehrers;
        }

        internal Lehrers GetKlassenleiter(List<string> beteiligteKlassen)
        {
            var beteiligteLehrers = new Lehrers();

            foreach (var klasse in this)
            {
                foreach (var beteiligteKlasse in beteiligteKlassen)
                {

                    if (klasse.NameUntis.StartsWith(beteiligteKlasse))
                    {
                        foreach (var klassenleitung in klasse.Klassenleitungen)
                        {
                            beteiligteLehrers.Add((from l in Lehrers where klassenleitung.Kürzel == l.Kürzel select l).FirstOrDefault());
                        }
                    }
                }
            }
            return beteiligteLehrers;
        }
    }
}