﻿using Microsoft.Exchange.WebServices.Data;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;

namespace Terminverwaltung
{
    public class Lehrers : List<Lehrer>
    {
        public Lehrers()
        {
        }

        public Lehrers(Raums raums, Periodes periodes)
        {
            using (OleDbConnection oleDbConnection = new OleDbConnection(Global.ConU))
            {
                try
                {
                    string queryString = @"SELECT DISTINCT 
Teacher.Teacher_ID, 
Teacher.Name, 
Teacher.Longname, 
Teacher.FirstName,
Teacher.Email,
Teacher.Flags,
Teacher.Title,
Teacher.ROOM_ID,
Teacher.Text2,
Teacher.Text3,
Teacher.PlannedWeek
FROM Teacher 
WHERE (((SCHOOLYEAR_ID)= " + Global.AktSjUnt + ") AND  ((TERM_ID)=" + periodes.Count + ") AND ((Teacher.SCHOOL_ID)=177659) AND (((Teacher.Deleted)=No))) ORDER BY Teacher.Name;";

                    OleDbCommand oleDbCommand = new OleDbCommand(queryString, oleDbConnection);
                    oleDbConnection.Open();
                    OleDbDataReader oleDbDataReader = oleDbCommand.ExecuteReader();
                    int i = 0;
                    using (var progress = new ProgressBar())
                    {
                        while (oleDbDataReader.Read())
                        {
                            Lehrer lehrer = new Lehrer()
                            {
                                IdUntis = oleDbDataReader.GetInt32(0),
                                Kürzel = Global.SafeGetString(oleDbDataReader, 1),
                                Nachname = Global.SafeGetString(oleDbDataReader, 2),
                                Vorname = Global.SafeGetString(oleDbDataReader, 3),
                                Mail = Global.SafeGetString(oleDbDataReader, 4),
                                Anrede = Global.SafeGetString(oleDbDataReader, 5) == "n" ? "Herr" : Global.SafeGetString(oleDbDataReader, 5) == "W" ? "Frau" : "",
                                Titel = Global.SafeGetString(oleDbDataReader, 6),
                                Raum = (from r in raums where r.IdUntis == oleDbDataReader.GetInt32(7) select r.Raumnummer).FirstOrDefault(),
                                Funktion = Global.SafeGetString(oleDbDataReader, 8),
                                Dienstgrad = Global.SafeGetString(oleDbDataReader, 9)
                            };

                            if (lehrer.Kürzel != "?" && lehrer.Kürzel != "LAT")
                            {
                                if (!lehrer.Mail.EndsWith("@berufskolleg-borken.de"))
                                    Console.WriteLine("Der Lehrer " + lehrer.Kürzel + " hat keine Mail-Adresse in Untis. Bitte in Untis eintragen.");
                                if (lehrer.Anrede == "")
                                    Console.WriteLine("Der Lehrer " + lehrer.Kürzel + " hat kein Geschlecht in Untis. Bitte in Untis eintragen.");
                                this.Add(lehrer);
                            }
                            i++;
                            progress.Report((double)i / 130);
                        };
                    }
                    

                    Console.WriteLine(("Lehrer " + ".".PadRight(this.Count / 150, '.')).PadRight(48, '.') + (" " + this.Count).ToString().PadLeft(4), '.');

                    oleDbDataReader.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    throw new Exception(ex.ToString());
                }
                finally
                {
                    oleDbConnection.Close();
                }
            }
        }

        internal void LehrerInMoodleKlassenkurseEinschreiben(Unterrichts unterrichts)
        {

            Encoding enc = Encoding.GetEncoding("UTF-8");

            string aa = @"lehrerInMoodleEinschreiben1.csv";

            using (StreamWriter writer = new StreamWriter(aa, true, enc))
            {
                Console.Write(("Die Datei wird geschrieben: " + aa).PadRight(75, '.'));

                writer.WriteLine(@"username;password;lastname;firstname;email;auth;course1;group1;role1;department;city;deleted");

                foreach (var lehrer in this)
                {
                    foreach (var item in (from u in unterrichts where u.LehrerKürzel == lehrer.Kürzel select u.KlasseKürzel).Distinct().ToList())
                    {
                        writer.WriteLine(lehrer.Kürzel.ToUpper() + ";123;" + lehrer.Nachname + ";" + lehrer.Vorname + ";" + lehrer.Mail + ";ldap;" + item + ";" + item + ";editingteacher;" + item + ";" + DateTime.Now.ToString("yyyyMMdd") + ";" + 0);
                    }
                }

                Console.WriteLine((" ok").PadLeft(30, '.'));
            }
        }

        internal string Fehlende(Excelzeilen excelzeilenQuelle)
        {
            string a = "";

            foreach (var item in this)
            {
                var fehlende = (from s in excelzeilenQuelle
                               from k in s.IVerantwortlich
                               where k.GetType() == typeof(Lehrer)
                               where ((Lehrer)k).Kürzel == item.Kürzel
                               select ((Lehrer)k)).FirstOrDefault();
                if (fehlende == null)
                {
                    a += item.Kürzel + ",";
                }
            }
            return a.TrimEnd(',');
        }
    }
}