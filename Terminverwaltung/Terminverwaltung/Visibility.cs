﻿namespace Terminverwaltung
{
    internal class Visibility
    {
        public int[] rows { get; internal set; }
        public int[] columns { get; internal set; }

        public Visibility(int[] rows, int[] columns)
        {
            this.rows = rows;
            this.columns = columns;
        }
    }
}