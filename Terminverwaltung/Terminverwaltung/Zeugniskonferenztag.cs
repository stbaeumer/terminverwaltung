﻿using Microsoft.Office.Interop.Excel;
using System.Collections.Generic;

namespace Terminverwaltung
{
    public class Zeugniskonferenztag : List<Zeugniskonferenzsitzung>
    {
        public List<Klasse> Klassen { get; private set; }
        public string Name { get; set; }

        public Zeugniskonferenztag(string name)
        {
            Name = name;
        }        
    }
}