﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;

namespace Terminverwaltung
{
    public class Anrechnungs : List<Anrechnung>
    {
        public Anrechnungs() { }

        public Anrechnungs(Periodes periodes)
        {            
            Console.WriteLine("Anrechnungen ...");

            using (OleDbConnection oleDbConnection = new OleDbConnection(Global.ConU))
            {
                try
                {
                    string queryString = @"SELECT CountValue.TEACHER_ID, CountValue.Text, Description.Name
FROM Description RIGHT JOIN CountValue ON Description.DESCRIPTION_ID = CountValue.DESCRIPTION_ID
WHERE (((CountValue.SCHOOL_ID)=177659) AND ((CountValue.SCHOOLYEAR_ID)=" + Global.AktSjUnt + ") AND ((Description.SCHOOLYEAR_ID)=" + Global.AktSjUnt + @") AND ((Description.SCHOOL_ID)=177659));

";                    
                    OleDbCommand oleDbCommand = new OleDbCommand(queryString, oleDbConnection);
                    oleDbConnection.Open();
                    OleDbDataReader oleDbDataReader = oleDbCommand.ExecuteReader();
                    
                    while (oleDbDataReader.Read())
                    {
                        int lehrerId = (oleDbDataReader.GetInt32(0));
                        string beschreibung = Global.SafeGetString(oleDbDataReader, 1); 
                        string kategorie = Global.SafeGetString(oleDbDataReader, 2);
                        
                        this.Add(new Anrechnung(lehrerId, beschreibung, kategorie));                      
                    };
                    oleDbDataReader.Close();                    
                }
                catch (Exception ex)
                {
                    Console.ReadKey();
                }
                finally
                {
                    oleDbConnection.Close();
                }
            }
        }
        public string SafeGetString(OleDbDataReader reader, int colIndex)
        {
            if (!reader.IsDBNull(colIndex))
                return reader.GetString(colIndex);
            return string.Empty;
        }
    }
}