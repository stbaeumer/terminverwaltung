﻿namespace Terminverwaltung
{
    public class Verteilergruppe
    {
        public string Name { get; private set; }
        public string Mail { get; private set; }
        public string Url { get; private set; }
        public string Langname { get; private set; }

        public Verteilergruppe(string name, string langname, string mail, string url)
        {
            Name = name;
            Langname = langname;
            Mail = mail;
            Url = url;
        }
    }
}