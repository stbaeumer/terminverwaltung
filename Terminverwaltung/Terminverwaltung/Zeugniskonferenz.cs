﻿using System;
using System.Collections.Generic;

namespace Terminverwaltung
{
    public class Zeugniskonferenzsitzung
    {
        public string Zk { get; private set; }
        public string Bereich { get; private set; }
        public List<Klasse> Klassen { get; private set; }
        public Raum Raum { get; private set; }

        public Zeugniskonferenzsitzung(string zk, string bereich, List<Klasse> klassen, Raum raum)
        {
            Zk = zk;
            Bereich = bereich;
            Klassen = klassen;
            Raum = raum;
        }
    }
}