﻿namespace Terminverwaltung
{
    internal class Optionen
    {
        public int last_editor { get; internal set; }
        public bool table_head { get; internal set; }
        public bool table_foot { get; internal set; }
        public string extra_css_classes { get; internal set; }
        public bool use_datatables { get; internal set; }
        public bool datatables_sort { get; internal set; }
        public bool datatables_filter { get; internal set; }
        public bool datatables_paginate { get; internal set; }
        public bool datatables_lengthchange { get; internal set; }
        public int datatables_paginate_entries { get; internal set; }
        public bool datatables_info { get; internal set; }
        public bool datatables_scrollx { get; internal set; }
        public string datatables_custom_commands { get; internal set; }
        public bool alternating_row_colors { get; internal set; }
        public bool row_hover { get; internal set; }
        public bool print_name { get; internal set; }
        public string print_name_position { get; internal set; }
        public bool print_description { get; internal set; }
        public string print_description_position { get; internal set; }

        public Optionen(int last_editor, bool table_head, bool v3, bool v4, bool v5, bool v6, string v7, bool v8, string v9, string v10, bool v11, bool v12, bool v13, bool v14, bool v15, int v16, bool v17, bool v18, string v19)
        {
            this.last_editor = last_editor;
            this.table_head = table_head;
            this.table_foot = v3;
            this.alternating_row_colors = v4;
            this.row_hover = v5;
            this.print_name = v6;
            this.print_name_position = v7;
            this.print_description = v8;
            this.print_description_position = v9;
            this.extra_css_classes = v10;
            this.use_datatables = v11;
            this.datatables_sort = v12;
            this.datatables_filter = v13;
            this.datatables_paginate = v14;
            this.datatables_lengthchange = v15;
            this.datatables_paginate_entries = v16;
            this.datatables_info = v17;
            this.datatables_scrollx = v18;
            this.datatables_custom_commands = v19;
        }
    }
}