﻿namespace Terminverwaltung
{
    public class Anrechnung
    {
        public int LehrerId { get; internal set; }
        public string Beschreibung { get; internal set; }
        public object Kategorie { get; private set; }

        public Anrechnung(int lehrerId, string beschreibung, string kategorie)
        {
            LehrerId = lehrerId;
            Beschreibung = beschreibung;
            Kategorie = kategorie;
        }
    }
}