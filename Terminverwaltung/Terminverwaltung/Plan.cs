﻿using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;

namespace Terminverwaltung
{
    public class Plan
    {
        public string Name { get; internal set; }
        public Termine Termine { get; set; }
        public Klasses Klassen { get; set; }
        public Unterrichts Unterrichts { get; set; }
        public Lehrers Lehrers { get; set; }
        public Feriens Feriens { get; set; }

        public Plan(string name, Klasses klassen, Unterrichts unterrichts, Lehrers lehrers, Feriens feriens)
        {
            Name = name;
            Termine = new Termine();
            Klassen = klassen;
            Unterrichts = unterrichts;
            Lehrers = lehrers;
            Feriens = feriens;
        }

        internal void GetTermine()
        {
            Termine termine = new Termine();

            using (StreamReader reader = new StreamReader(@"\\fs01\Schulverwaltung\Terminverwaltung\" + Name, System.Text.Encoding.Default, false))
            {
                DateTime datum = new DateTime();

                while (true)
                {
                    string line = reader.ReadLine();
                    try
                    {
                        Termin termin = new Termin();
                        var x = line.Split('\t');
                        
                        Lehrers beteiligte = new Lehrers();

                        if (x[3] != "" && x[2] == "")
                        {
                            Console.Write(x[3] + "[TT.MM.JJJJ]");
                            
                            do
                            {
                                datum = DateTime.ParseExact(Console.ReadLine(), "dd.MM.yyyy", CultureInfo.InvariantCulture);

                            } while (datum == null);
                        }
                        else
                        {
                            datum = ErrechneDatum(datum, Convert.ToInt32(x[2]));
                        }

                        termin.Name = x[0];
                        termin.DatumUhrzeit = datum;                        
                        termin.BeteiligteLehrer = GetPersonen(x[5],x[6]);
                        termine.Add(termin);
                    }
                    catch (Exception)
                    {
                    }

                    if (line == null)
                    {
                        break;
                    }
                }

                Console.WriteLine((" " + termine.Count.ToString()).PadLeft(30, '.'));
            }
            this.Termine = termine;
        }

        internal void ErstelleAppointments()
        {
            throw new NotImplementedException();
        }

        internal void ErstelleExcel()
        {
            string dateiNameNeu = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + Name;

            Application excel = new Application();
            Workbook workbook = excel.Workbooks.Open(@"\\fs01\Schulverwaltung\Terminverwaltung\" + Name.Replace(".csv",".xlsx"));
            Worksheet worksheet = (Worksheet)workbook.Worksheets.get_Item(1);
            
            int spalte = 2;
            int zeile = 1;
            
            worksheet.Cells[zeile, spalte] = "Stand: " + DateTime.Now.ToShortDateString();

            spalte = 6;
            zeile = 2;
                        
            while (File.Exists(dateiNameNeu))
            {                
                
                    Console.Write("Die Datei " + Path.GetFileName(dateiNameNeu) + " existiert bereits. Bitte löschen.'x' für automatisches Löschen. 'a' für alle Löschen. Dann ENTER.");
                    var k = (Console.ReadKey(true)).Key;
                

                if (k == ConsoleKey.X)
                {
                    File.Delete(dateiNameNeu);
                }
                else
                {
                    Process.Start(Path.GetDirectoryName(dateiNameNeu));
                }
            }
            workbook.SaveAs(dateiNameNeu);

            workbook.Close();
            System.Runtime.InteropServices.Marshal.ReleaseComObject(workbook);
            excel.Quit();

            Process.Start(Path.GetDirectoryName(dateiNameNeu));
        }

        private Lehrers GetPersonen(string beteiligtePersonen, string beteiligteKlassenString)
        {
            Lehrers beteiligte = new Lehrers();

            List<string> beteiligteKlassen = new List<string>();

            foreach (var k in beteiligteKlassenString.Split(','))
            {
                beteiligteKlassen.Add(k);
            }

            if (beteiligtePersonen.Contains("BL"))
            {
                beteiligte.AddRange(Klassen.GetBereichleiter(beteiligteKlassen));
            }
            if (beteiligtePersonen.Contains("LUL"))
            {
                beteiligte.AddRange(Unterrichts.GetLehrer(beteiligteKlassen));
            }
            if (beteiligtePersonen.Contains("Stellv"))
            {
                beteiligte.Add((from l in Lehrers where l.Kürzel == "BM" select l).FirstOrDefault());
            }
            if (beteiligtePersonen.Contains("SL"))
            {
                beteiligte.Add((from l in Lehrers where l.Kürzel == "SUE" select l).FirstOrDefault());                
            }
            return beteiligte;
        }

        private DateTime ErrechneDatum(DateTime ausgehendVonDatum, int anzahlTage)
        {
            // Ferientage und Wochenenden werden abgezogen

            int step = anzahlTage / Math.Abs(anzahlTage);

            DateTime tag = ausgehendVonDatum;

            for (int i = 0; i < Math.Abs(anzahlTage); i++)
            {
                tag = tag.AddDays(step);

                // Wenn ein Wochenende oder Ferientag zwischen den Terminen liegt, verlängert sich die Frist.

                if ((tag.DayOfWeek == DayOfWeek.Saturday) || (tag.DayOfWeek == DayOfWeek.Sunday) || Feriens.IstFerientag(tag))
                {
                    anzahlTage = anzahlTage + step;
                }
            }
            return ausgehendVonDatum.AddDays(anzahlTage);
        }
    }
}