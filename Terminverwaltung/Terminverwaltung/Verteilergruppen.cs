﻿using System;
using System.Collections.Generic;

namespace Terminverwaltung
{
    public class Verteilergruppen : List<Verteilergruppe>
    {
        public Verteilergruppen()
        {
            this.Add(new Verteilergruppe("Kollegium", "", "kollegium@berufskolleg-borken.de", "https://www.berufskolleg-borken.de/das-kollegium/#Bild"));
            this.Add(new Verteilergruppe("Berufsfachschule Fachrichtung Sozialassistent/in", "", "", "https://www.berufskolleg-borken.de/bildungsgange/berufsfachschule/sozialassistenten/"));
            this.Add(new Verteilergruppe("Berufsfachschule für Technik/Naturwissenschaften", "", "", "https://www.berufskolleg-borken.de/bildungsgange/berufsschule/berufsgrundschuljahr/"));
            this.Add(new Verteilergruppe("Berufsfachschule für Technik/Naturwissenschaften (einjährig)", "", "", "https://www.berufskolleg-borken.de/bildungsgange/berufsschule/berufsgrundschuljahr/"));
            this.Add(new Verteilergruppe("Berufsfachschule Wirtschaft und Verwaltung (einjährig)", "", "", "https://www.berufskolleg-borken.de/bildungsgange/berufsschule/berufsgrundschuljahr-wirtschaft-und-verwaltung-einjahrig/"));

            this.Add(new Verteilergruppe("Fachoberschule für Gesundheit und Soziales", "", "", "https://www.berufskolleg-borken.de/bildungsgange/fachoberschulen/fachoberschule-fur-sozial-und-gesundheitswesen-schwerpunkt-gesundheitswesen/"));
            this.Add(new Verteilergruppe("Fachoberschule Technik", "", "", "https://www.berufskolleg-borken.de/bildungsgange/fachoberschulen/technik/"));
            this.Add(new Verteilergruppe("Fachoberschule für Wirtschaft und Verwaltung", "", "", "https://www.berufskolleg-borken.de/bildungsgange/fachoberschulen/wirtschaft/"));
            
            this.Add(new Verteilergruppe("BS", "Berufsfachschule Fachrichtung Sozialassistent/in", "", "https://www.berufskolleg-borken.de/bildungsgange/berufsfachschule/sozialassistenten/"));
            //this.Add(new Verteilergruppe("BSO", "", "https://www.berufskolleg-borken.de/bildungsgange/berufsfachschule/sozialassistenten/"));
            this.Add(new Verteilergruppe("A01", "", "", "https://www.berufskolleg-borken.de/bildungsgange/berufsschule/"));
            this.Add(new Verteilergruppe("Berufsschule", "", "", "https://www.berufskolleg-borken.de/bildungsgange/berufsschule/"));
            this.Add(new Verteilergruppe("FOS", "", "", "https://www.berufskolleg-borken.de/bildungsgange/fachoberschulen/"));
            this.Add(new Verteilergruppe("C03", "", "", "https://www.berufskolleg-borken.de/bildungsgange/fachoberschulen/"));
            //this.Add(new Verteilergruppe("FS" + ( DateTime.Now.Month  DateTime.Now.Year.ToString("YY")) , "", "", "https://www.berufskolleg-borken.de/bildungsgange/fachoberschulen/fachoberschule-fur-sozial-und-gesundheitswesen-schwerpunkt-gesundheitswesen/"));
            this.Add(new Verteilergruppe("FS", "", "", "https://www.berufskolleg-borken.de/bildungsgange/fachoberschulen/fachoberschule-fur-sozial-und-gesundheitswesen-schwerpunkt-gesundheitswesen/"));
            //this.Add(new Verteilergruppe("12M", "", "https://www.berufskolleg-borken.de/bildungsgange/fachoberschulen/technik/"));
            //this.Add(new Verteilergruppe("12W", "", "https://www.berufskolleg-borken.de/bildungsgange/fachoberschulen/wirtschaft/"));
            this.Add(new Verteilergruppe("HB", "", "", "https://www.berufskolleg-borken.de/hoehere-berufsfachschule/"));
            this.Add(new Verteilergruppe("C02", "", "", "https://www.berufskolleg-borken.de/hoehere-berufsfachschule/"));
            this.Add(new Verteilergruppe("HBG", "", "", "https://www.berufskolleg-borken.de/bildungsgange/hohere-berufsfachschulen/gesundheit-soziales/schwerpunkt-gesundheit/"));
            this.Add(new Verteilergruppe("BFS", "", "", "https://www.berufskolleg-borken.de/bildungsgange/berufsfachschule/"));                       
            //this.Add(new Verteilergruppe("HBFGU", "", "", "https://www.berufskolleg-borken.de/bildungsgange/hohere-berufsfachschulen/gesundheit-soziales/schwerpunkt-gesundheit/"));
            //this.Add(new Verteilergruppe("HBFGO", "", "", "https://www.berufskolleg-borken.de/bildungsgange/hohere-berufsfachschulen/gesundheit-soziales/schwerpunkt-gesundheit/"));
            this.Add(new Verteilergruppe("HBT", "", "", "https://www.berufskolleg-borken.de/bildungsgange/hohere-berufsfachschulen/hohere-berufsfachschule-technik/"));
            //this.Add(new Verteilergruppe("HBTO", "", "https://www.berufskolleg-borken.de/bildungsgange/hohere-berufsfachschulen/hohere-berufsfachschule-technik/"));
            //this.Add(new Verteilergruppe("HBTU", "", "https://www.berufskolleg-borken.de/bildungsgange/hohere-berufsfachschulen/hohere-berufsfachschule-technik/"));
            this.Add(new Verteilergruppe("HBW", "", "", "https://www.berufskolleg-borken.de/bildungsgange/hohere-berufsfachschulen/wirtschaft-verwaltung/"));
            //this.Add(new Verteilergruppe("HHO", "", "", "https://www.berufskolleg-borken.de/bildungsgange/hohere-berufsfachschulen/wirtschaft-verwaltung/"));
            //this.Add(new Verteilergruppe("HHU", "", "", "https://www.berufskolleg-borken.de/bildungsgange/hohere-berufsfachschulen/wirtschaft-verwaltung/"));
            this.Add(new Verteilergruppe("B06", "", "", "https://www.berufskolleg-borken.de/bildungsgange/berufsfachschule/"));
            this.Add(new Verteilergruppe("B07", "", "", "https://www.berufskolleg-borken.de/bildungsgange/berufsfachschule/"));            
            //this.Add(new Verteilergruppe("BT1", "", "https://www.berufskolleg-borken.de/bildungsgange/berufsschule/berufsgrundschuljahr/"));
            //this.Add(new Verteilergruppe("BW1", "", "https://www.berufskolleg-borken.de/bildungsgange/berufsschule/berufsgrundschuljahr-wirtschaft-und-verwaltung-einjahrig/"));
            this.Add(new Verteilergruppe("Gym", "", "", "https://www.berufskolleg-borken.de/bildungsgange/berufliches-gymnasium/"));
            this.Add(new Verteilergruppe("D02", "", "", "https://www.berufskolleg-borken.de/bildungsgange/berufliches-gymnasium/"));
            this.Add(new Verteilergruppe("GG", "", "", "https://www.berufskolleg-borken.de/bildungsgange/berufliches-gymnasium/gymnasium-erziehung-soziales/"));
            this.Add(new Verteilergruppe("GT", "", "", "https://www.berufskolleg-borken.de/bildungsgange/berufliches-gymnasium/technikgymnasium/"));
            this.Add(new Verteilergruppe("GW", "", "", "https://www.berufskolleg-borken.de/bildungsgange/berufliches-gymnasium/wirtschaftsgymnasium/"));
            this.Add(new Verteilergruppe("BG", "", "", "https://www.berufskolleg-borken.de/bildungsgange/berufliches-gymnasium/"));
            //this.Add(new Verteilergruppe("WB", "", "", "https://www.berufskolleg-borken.de/bildungsgange/berufsschule/ausbildungsberuf-buerokaufmann-kauffrau/"));
            //this.Add(new Verteilergruppe("WB", "", "", "https://www.berufskolleg-borken.de/bildungsgange/berufsschule/ausbildungsberuf-buerokaufmann-kauffrau/"));
            this.Add(new Verteilergruppe("WB", "", "", "https://www.berufskolleg-borken.de/bildungsgange/berufsschule/ausbildungsberuf-buerokaufmann-kauffrau/"));
            this.Add(new Verteilergruppe("Erweiterte Schulleitung", "", "erweiterte-schulleitung@berufskolleg-borken.de", "https://www.berufskolleg-borken.de/schulleitung/#ESL"));
            this.Add(new Verteilergruppe("Bereichsleitung", "", "erweiterte-schulleitung@berufskolleg-borken.de", "https://www.berufskolleg-borken.de/schulleitung/"));
            this.Add(new Verteilergruppe("Schulbüro", "", "post@berufskolleg-borken.de", "https://www.berufskolleg-borken.de/schulbuero/"));
            this.Add(new Verteilergruppe("Schulleitung", "", "schulleitung@berufskolleg-borken.de", "https://www.berufskolleg-borken.de/schulleitung/#SL"));
            this.Add(new Verteilergruppe("Stundenplanung", "", "stundenplanung@berufskolleg-borken.de", ""));
            this.Add(new Verteilergruppe("Vertretungsplanung", "", "vertretungsplanung@berufskolleg-borken.de", ""));
            this.Add(new Verteilergruppe("Stellvertretender Schulleiter", "", "stefan.baeumer@berufskolleg-borken.de", "https://www.berufskolleg-borken.de/schulleitung/#SL"));
            this.Add(new Verteilergruppe("Schulleiterin", "", "annette.suehling@berufskolleg-borken.de", "https://www.berufskolleg-borken.de/schulleitung/#SL"));
            this.Add(new Verteilergruppe("Berufliches Gymnasium", "", "", "https://www.berufskolleg-borken.de/bildungsgange/berufliches-gymnasium/"));
        }
    }
}